//
//  TextFieldView.swift
//  BluArmor-App
//
//  Created by Rahul Gaur on 28/02/22.
//

import SwiftUI
import Combine

struct TextFieldView: View {
    @Binding var text:String
    
    let label:String
    let suggestionText:String
    let showCharacterCount:Bool
    let maxCharacterCount:Int
    
    var body: some View {
        VStack(alignment: .leading, spacing: 8){
            Text(label)
                .font(.system(.footnote))
                .fontWeight(.semibold)
                .foregroundColor(ColorTextPrimaryAccent)
                .padding(.leading, 12)
            
            TextField(suggestionText, text:$text )
                .onReceive(Just(self.text)) { inputValue in
                    if self.text.count > maxCharacterCount {
                        self.text = String(self.text.prefix(maxCharacterCount))
                    }
                }
            //                .onChange(of: self.text, perform: {
            //                    if $0.count > maxCharacterCount {
            //                        self.text = String($0.prefix(maxCharacterCount))
            //                    }
            
            //                })
                .font(.system(.body))
                .foregroundColor(Color(0x1FFFFFFF))
                .padding(.vertical, 18)
                .padding(.horizontal, 10)
                .background(Color(0xFF17324A))
                .cornerRadius(10)
            
            Text("\(text.count)/\(maxCharacterCount)")
                .font(.system(.caption))
                .fontWeight(.semibold)
                .foregroundColor(ColorTextPrimaryAccent)
                .padding(.trailing, 12)
                .frame(maxWidth: .infinity,alignment: .trailing)
        }
    }
}
struct TextFieldViewDisabled: View {
    
    let text:String
    let label:String
    
    var body: some View {
        VStack(alignment: .leading, spacing: 8){
            Text(label)
                .font(.system(.footnote))
                .fontWeight(.semibold)
                .foregroundColor(ColorTextPrimaryAccent)
                .padding(.leading, 12)
            
            Text(text)
                .font(.system(.body))
                .foregroundColor(Color(0x1FFFFFFF))
                .padding(.vertical, 18)
                .padding(.horizontal, 10)
                .frame(maxWidth: .infinity, alignment: .leading)
                .background(Color(0xFF17324A))
                .cornerRadius(10)
            
        }
    }
}
