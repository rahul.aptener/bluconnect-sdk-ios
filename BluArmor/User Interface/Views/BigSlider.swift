//
//  BigSlider.swift
//  BluArmor-App
//
//  Created by Rahul Gaur on 21/01/22.
//

import Foundation
import SwiftUI

struct BigSliderView: View {
    let maxLevel:Float
    var level:Float
    
    @State private var isEditing = false

    let onProgressCallBack:(Int)->()
    
    //    @Binding var percentage: Float // or some value binded

    
    var body: some View {
        GeometryReader { geometry in
            // TODO: - there might be a need for horizontal and vertical alignments
            ZStack(alignment: .leading) {
                Rectangle()
                    .foregroundColor(Color(hex:0xff414249))
                Rectangle()
                    .foregroundColor(Color(hex:0xff0087FF))
                    .frame(width: geometry.size.width * CGFloat(level / maxLevel))
            }
            .cornerRadius(8)
            .gesture(DragGesture(minimumDistance: 0)
            .onChanged({ value in
                isEditing = false

                let progress = min(max(0, Float(value.location.x / geometry.size.width) * maxLevel), maxLevel)
                
                
                if(Int(progress) != Int(level)){
                    self.onProgressCallBack(Int(progress))
                }
//                self.level = progress
            }).onEnded({
                _ in
                
                isEditing = true
            }))
        }
    }
}


struct SwipeView<Content:View>: View {
    
    @State private var offset: CGSize = .zero
    let content: Content
    let action:()->()


    var body: some View {
        GeometryReader{ g in
            VStack(alignment: .center){
                Spacer()
                content
//                    .background(Color.red)
//                    .padding(.bottom, 20) //corner radius part
//                    .cornerRadius(20) //corner radius part
//                    .padding(.bottom, -20) //corner radius part
                    .frame( minWidth: 0, maxWidth: .infinity, minHeight: 0)
            }
//                .padding(.horizontal, 8)
                .offset(offset)
                .gesture(
                    DragGesture()
                        .onChanged({ v in
                            // "\(v.translation)".logD(tag: "DRAG")
                            if v.translation.width >= 0{
                                offset = CGSize(width: v.translation.width, height: 0)
                                if (offset.width * 100 / g.size.width) > 70{
                                    action()
                                    offset = CGSize(width: g.size.width, height: 0)
                                }
                            }
                        })
                        .onEnded({ v in
                            offset = .zero
                        })
                )
                .animation(.default)
        }
    }
}
