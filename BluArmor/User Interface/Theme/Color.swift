//
//  Color.swift
//  BluArmor-App
//
//  Created by Rahul Gaur on 21/01/22.
//

import Foundation
import SwiftUI


let ColorPositive = Color(hex:0xFF4de77c)
let ColorNegative = Color(hex:0xFFff4f64)
let ColorNegativeLight = Color(hex:0xFFFFA3AE)
let ColorDarkShadow = Color(hex:0xFF4E4E4E)
let ColorShark = Color(hex:0xFF242428)
let ColorSharkAccent = Color(hex:0xFF3C3C45)
let ColorSharkDark = Color(hex:0xFF242428)

let ColorTextPrimary = Color(hex:0xFFFFFFFF)
let ColorTextPrimaryAccent = Color(hex:0xB3FFFFFF)
let ColorTextSecondary = Color(hex:0xFFFFFFFF)
let ColorTextSecondaryAccent = Color(hex:0xFFFFFFFF)

