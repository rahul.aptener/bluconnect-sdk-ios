//
//  BackgroundView.swift
//  BluArmor-App
//
//  Created by Rahul Gaur on 20/01/22.
//

import Foundation
import SwiftUI


struct BaseGradientView: View {
    var body: some View {
        LinearGradient(
            gradient: Gradient(stops: [
                Gradient.Stop(color: Color(0x194A74, alpha: 1.0), location: 0.0),
                Gradient.Stop(color: Color(0x000000), location: 0.3)
            ]),
            startPoint: .top,
            endPoint: .bottom)
            .preferredColorScheme(.dark)
    }
}


struct SplashGradientView: View {
    var body: some View {
        LinearGradient(
            gradient: Gradient(stops: [
                Gradient.Stop(color: Color(0x2E5A80, alpha: 1.0), location: 0.0),
                Gradient.Stop(color: Color(0x254C6D, alpha: 1.0), location: 0.3),
                Gradient.Stop(color: Color(0x031321, alpha: 1.0), location: 1.0)
            ]),
            startPoint: .top,
            endPoint: .bottom)
            .preferredColorScheme(.dark)
    }
}
