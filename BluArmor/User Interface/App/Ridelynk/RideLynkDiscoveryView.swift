//
//  RideLynkDiscoveryView.swift
//  BluArmor-App
//
//  Created by Rahul Gaur on 26/01/22.
//

import SwiftUI
import BluConnect

struct RideLynkDiscoveryContentView: View {
    @ObservedObject var vm:BluArmorAppViewModel
    var body: some View {
        BaseGradientView()
            .edgesIgnoringSafeArea(.all)
        
            .overlay(
                VStack(alignment: .center, spacing: 12){
                    
                    DeviceInfoCardCollapsed(
                        deviceInfo: vm.deviceInfo ,
                        battery: vm.batteryLevel
                    )
                    
                    HStack(alignment: .center, spacing: 12){
                        
                        Text("Available Riders".uppercased())   .font(.system(.caption))
                            .fontWeight(.semibold)
                            .foregroundColor(ColorTextPrimaryAccent)
                            .padding(.leading, 12)
                        
                        Spacer()
                        
                        if let rideLynkState = vm.rideLynkState,  case RideLynkState.Discovery(_) = rideLynkState{
                            Button(action:{
                                vm.rideLynkAction(action: .StopDiscovery)
                            }){
                                HStack(spacing: 4){
                                    Spacer().frame(width: 12)
                                    Text( "Stop Discovery".uppercased())
                                        .font(.system(.caption))
                                        .fontWeight(.semibold)
                                        .foregroundColor(ColorShark)
                                        .padding(.vertical, 9)
                                    // TODO Add progress bar
                                    //                                    ProgressView()
                                    //                                        .foregroundColor(ColorShark)
                                    //                                        .progressViewStyle(CircularProgressViewStyle(tint: ColorShark))
                                    //                                        .scaleEffect(0.7)
                                    
                                    Spacer().frame(width: 4)
                                    
                                }
                                
                                .background(Color.white)
                                .cornerRadius(100)
                            }
                        }else if vm.rideLynkState == RideLynkState.Idle {
                            Button(action:{
                                vm.rideLynkAction(action: .StartDiscovery)
                            }){
                                HStack(spacing: 4){
                                    Spacer().frame(width: 12)
                                    Text( "Start Discovery".uppercased())
                                        .font(.system(.caption))
                                        .fontWeight(.semibold)
                                        .foregroundColor(ColorShark)
                                        .padding(.vertical, 9)
                                    
                                    Spacer().frame(width: 12)
                                    
                                }
                                .background(Color.white)
                                .cornerRadius(100)
                            }
                        }
                    }.padding(.horizontal, 16)
                        .frame(maxWidth: .infinity)
                    
//                    ScrollView(.vertical) {
//                    Text("\( String(describing:Array(vm.rideLynkDevices).map{ $0.name }))")

                        List{
                            
                            
                            ForEach(vm.rideLynkDevices.unique(), id: \.macAddress){ device in
                                DiscoveredDeviceView(
                                    label: device.name ?? device.macAddress,
                                    action:{  vm.rideLynkAction(action: .Connect(device: device))  }
                                )
                            }
                        }
                        //                        LazyVStack(alignment: .center, spacing: 12){
                        //                            ForEach(Array(vm.rideLynkDevices), id: \.self){ device in
                        //                                DiscoveredDeviceView(
                        //                                    label: device.name ?? device.macAddress,
                        //                                    action:{  vm.rideLynkAction(action: .Connect(device: device))  }
                        //                                )
                        //                                    .listRowSeparator(.hidden)
                        //                                    .listRowInsets(EdgeInsets(top:0, leading:0, bottom: 8, trailing: 0))
                        //                            }
                        //                        }
//                    }
                    
                    
                    if let state = vm.rideLynkState,
                       case RideLynkState.Connected(let rideLynkDevice) = state{
                        VStack(alignment: .center, spacing: 4){
                            Image("helmet")
                                .resizable()
                                .scaledToFit()
                                .frame( height: 36, alignment: .center)
                            
                            Text( "Connected".uppercased())
                                .font(.system(.caption))
                                .fontWeight(.semibold)
                                .foregroundColor(ColorTextPrimary)
                                .padding(.vertical, 9)
                            
                            Text(rideLynkDevice.name)
                                .font(.system(.footnote))
                                .fontWeight(.semibold)
                                .foregroundColor(ColorTextPrimary)
                                .padding(.vertical, 9)
//                            
//                            if let name = {
//                                
//                            }else{Text(rideLynkDevice.macAddress)
//                                    .font(.system(.footnote))
//                                    .fontWeight(.semibold)
//                                    .foregroundColor(ColorTextPrimary)
//                                    .padding(.vertical, 9)
//                            }
                            
                            HStack{
                                
                                Button(action:{
                                    vm.rideLynkAction(action: .Disconnect)
                                }){
                                    Text( "Disconnect".uppercased())
                                        .font(.system(.caption))
                                        .fontWeight(.semibold)
                                        .foregroundColor(.white)
                                        .padding(.vertical, 12)
                                        .padding(.horizontal, 16)
                                        .background(ColorNegative)
                                        .cornerRadius(100)
                                }
                                
                                
                                Button(action:{
                                    vm.rideLynkAction(action: .StartCall)
                                }){
                                    Text( "Start Call".uppercased())
                                        .font(.system(.caption))
                                        .fontWeight(.semibold)
                                        .foregroundColor(.white)
                                        .padding(.vertical, 12)
                                        .padding(.horizontal, 16)
                                        .frame(maxWidth:.infinity)
                                        .background(ColorPositive)
                                        .cornerRadius(100)
                                }
                            }
                        }
                        .padding(12)
                        .frame(maxWidth: .infinity)
                        .background(ColorShark)
                        .cornerRadius(10)
                        .padding(.horizontal, 16)
                        
                    }else if let state = vm.rideLynkState,
                             case RideLynkState.ActiveCall(let rideLynkDevice) = state{
                        
                        VStack(alignment: .center, spacing: 4){
                            Image("helmet")
                                .resizable()
                                .scaledToFit()
                                .frame( height: 36, alignment: .center)
                            
                            Text( "Connected".uppercased())
                                .font(.system(.caption))
                                .fontWeight(.semibold)
                                .foregroundColor(ColorTextPrimary)
                                .padding(.vertical, 9)
                            
                            
                            Text(rideLynkDevice.name.uppercased())
                                .font(.system(.footnote))
                                .fontWeight(.semibold)
                                .foregroundColor(ColorTextPrimary)
                                .padding(.vertical, 9)
                            
                            
                            HStack{
                                Button(action:{
                                    vm.rideLynkAction(action: .Disconnect)
                                }){
                                    Text( "Disconnect".uppercased())
                                        .font(.system(.caption))
                                        .fontWeight(.semibold)
                                        .foregroundColor(.white)
                                        .padding(.vertical, 12)
                                        .padding(.horizontal, 16)
                                        .background(ColorNegative)
                                        .cornerRadius(100)
                                }
                                
                                Button(action:{
                                    vm.rideLynkAction(action: .StopCall)
                                }){
                                    Text( "End Call".uppercased())
                                        .font(.system(.caption))
                                        .fontWeight(.semibold)
                                        .foregroundColor(.white)
                                        .padding(.vertical, 12)
                                        .padding(.horizontal, 16)
                                        .frame(maxWidth:.infinity)
                                        .background(ColorNegative)
                                        .cornerRadius(100)
                                }
                                
                            }
                        }
                        .padding(12)
                        .frame(maxWidth: .infinity)
                        .background(ColorShark)
                        .cornerRadius(10)
                        .padding(.horizontal, 16)
                        
                    }
                    else{
                        HStack(){
                            Text("Speed Dial".uppercased())   .font(.system(.caption))
                                .fontWeight(.semibold)
                                .foregroundColor(ColorTextPrimaryAccent)
                                .padding(.leading, 16+12)
                        }
                        .frame(maxWidth: .infinity, alignment: .leading)
                        
                        SpeedDialContactsView(contacts: $vm.speedDialDevices, action: { vm.connectSpeedDialDevice(speedDialDevice: $0) })
                    }
                    
                    // For the visibility of centent under FAB
                    Spacer(minLength: 0).frame( width:0, height: 0)
                    
                    
                }.frame( alignment: .center )
                
            )
    }
}

struct RideLynkDiscoveryView: View {
    @ObservedObject var vm:BluArmorAppViewModel
    
    var body: some View {
        if #available(iOS 14.0, *) {
            RideLynkDiscoveryContentView(vm: vm)
                .navigationBarTitleDisplayMode(.inline)
                .toolbar{
                    ToolbarItem(placement: .navigationBarLeading) {
                        Button(action:{
                            self.vm.popBack()
                        }){
                            Image("back.nav")
                                .resizable()
                                .scaledToFit()
                                .frame( height: 44, alignment: .center)
                        }}
                }
        }
        else{
            NavigationView{
            RideLynkDiscoveryContentView(vm: vm)
                .navigationBarTitle("", displayMode: .inline)
                .navigationBarItems(leading: Button(action:{
                    self.vm.popBack()
                }){
                    Image("back.nav")
                        .renderingMode(.original)
                        .resizable()
                        .scaledToFit()
                        .frame( height: 44, alignment: .center)
                })}
        }
    }
}

struct DeviceInfoCardCollapsed: View {
    var deviceInfo:DeviceDetails?
    var battery:BatteryLevel?
    
    var body: some View {
        ZStack(alignment: .topLeading){
            LinearGradient(
                gradient: Gradient(colors: [Color(hex: 0xFF2E5A80),Color(hex: 0xFF254C6D),Color(hex: 0xFF0C2941)]), startPoint: .leading, endPoint: .trailing)
            .cornerRadius(8)
            .padding(.top ,18)
            
            Image("Sx20Small")
                .resizable()
                .aspectRatio( contentMode:.fit)
                .scaledToFit()
                .frame(width: 78, alignment: .leading)
            
            VStack(alignment: .leading, spacing: 10){
                HStack(alignment: .top, spacing: 4){
                    VStack(alignment: .leading){
                        Text(deviceInfo?.deviceName ?? "Disconnected")
                            .font(.system(.subheadline)).fontWeight(.semibold)
                            .foregroundColor(ColorTextPrimary)
                        if let modelLabel = deviceInfo?.model.label{
                            Text(modelLabel)
                                .font(.system(.caption))
                                .fontWeight(.regular)
                                .foregroundColor(ColorTextPrimaryAccent)
                        }
                        
                    }
                    .frame(
                        maxWidth: .infinity,
                        maxHeight: .infinity,
                        alignment: .leading
                    )
                    
                    if let batt = battery{
                        Text("\(batt.level)%").font(.system(.caption)).fontWeight(.semibold)
                            .foregroundColor(ColorTextPrimary)
                    }
                    
                }
                .padding(.leading, 78)
                .padding(.trailing, 16)
                
            }
            .padding(.top ,18)
            .padding(.vertical, 10)
        }
        .padding(.horizontal, 16)
        .fixedSize(horizontal: false, vertical: true)
    }
}




struct DiscoveredDeviceView: View {
    let label:String
    let action:()->Void
    
    var body: some View {
        HStack(alignment: .center, spacing: 12){
            
            Image("helmet")
                .resizable()
                .scaledToFit()
                .frame( width: 24,height: 24,  alignment: .center)
            
            Text(label)
                .font(.system(.subheadline))
                .fontWeight(.semibold)
                .foregroundColor(ColorTextPrimaryAccent)
                .padding(.leading, 0)
                .frame(maxWidth: .infinity,alignment: .leading)
            
            Button(action:self.action){
                Image(systemName: "phone.circle.fill")
                    .resizable()
                    .aspectRatio( contentMode:.fit)
                    .scaledToFit()
                    .padding(.horizontal, 6)
                    .foregroundColor(ColorPositive)
                    .frame( width: 40,height: 40, alignment: .center)
                    .padding(5)
            }
        }
        .padding(.leading, 12)
        .background(Color(0x1f1f1f))
        .cornerRadius(10)
        .padding(.horizontal, 16)
        
        //
    }
}

//struct RideLynkDiscoveryView_Previews: PreviewProvider {
//    static var previews: some View {
//        RideLynkDiscoveryView()
//    }
//}
