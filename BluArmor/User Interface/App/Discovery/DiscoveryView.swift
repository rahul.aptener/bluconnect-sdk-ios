//
//  DiscoveryView.swift
//  BluArmor-App
//
//  Created by Rahul Gaur on 23/01/22.
//

import Foundation
import SwiftUI

struct DiscoveryContentView: View {
    @ObservedObject var vm:BluArmorAppViewModel 
    
    var body: some View {
        BaseGradientView()
            .edgesIgnoringSafeArea(.all)
            .overlay(
                VStack(alignment: .center, spacing: 12){
                    VStack(alignment: .leading){
                        Text("Looking for BluArmor")
                            .font(.system(.largeTitle))
                            .fontWeight(.bold)
                            .multilineTextAlignment(.leading)
                            .lineLimit(2)
                            .foregroundColor(ColorTextPrimary)
                        Text("Make sure BluArmor is turned On")
                            .font(.system(.callout))
                            .fontWeight(.regular)
                            .foregroundColor(ColorTextPrimaryAccent)
                        
                        Spacer()
                        
                        Image("sx20")
                            .resizable()
                            .frame(width: 320, height: 320)
                        
//
//                        Image("sx20")
//                            .renderingMode(.original)
//                            .resizable()
//                            .scaledToFit()
//                            .frame(width: 320, alignment: .center)
//                            .frame(
//                                maxWidth: .infinity,
//                                maxHeight: .infinity,
//                                alignment: .center
//                            )
//                            .aspectRatio( contentMode:.fit)

                        
                        Spacer()
                    }
                    .frame(
                        maxWidth: .infinity,
                        maxHeight: .infinity,
                        alignment: .leading
                    )
                    .padding(.horizontal, 32)
                    
                    Spacer(minLength: 0).frame( width:0, height: 52)
                    
                    
                }.frame( alignment: .center )
            )
    }
}

struct DiscoveryView: View {
    @ObservedObject var vm:BluArmorAppViewModel
    
    var body: some View {
        if #available(iOS 14.0, *) {
            DiscoveryContentView(vm:vm)
                .navigationBarTitle("", displayMode: .inline)
                .navigationBarItems(trailing: Button(action:{
                    self.vm.navigateTo(appDestination: .PreferenceDest)
                }){
                    Image("preference")
                        .renderingMode(.original)
                        .resizable()
                        .scaledToFit()
                        .frame( height:44, alignment:.trailing)
                })
        }
        else{
            NavigationView{
                DiscoveryContentView(vm:vm)
                    .navigationBarTitle("", displayMode: .inline)
                    .navigationBarItems(trailing: Button(action:{
                        self.vm.navigateTo(appDestination: .PreferenceDest)
                    }){
                        Image("preference")
                            .renderingMode(.original)
                            .resizable()
                            .scaledToFit()
                            .frame( height:44, alignment:.trailing)
                    })
            }
        }
    }
}

//
//struct DiscoveryView_Previews: PreviewProvider {
//    static var previews: some View {
////        DiscoveryView().environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
////            .previewInterfaceOrientation(.portrait)
//    }
//}
