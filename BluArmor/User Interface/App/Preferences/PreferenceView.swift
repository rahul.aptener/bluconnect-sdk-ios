//
//  PreferenceView.swift
//  BluArmor-App
//
//  Created by Rahul Gaur on 27/01/22.
//

import SwiftUI
import Combine
import BluConnect

struct PreferenceContentView: View {
    @ObservedObject var vm:BluArmorAppViewModel
    
    @State var visible = true
    @State var activePrefPosition:Int = -1
    
    
    @State var phoneAutoAnswer: PhoneCallAutoAnswerRadioOption = .AUTO_ANSWER_MANUALLY
    @State var phoneAutoReject: PhoneCallAutoRejectRadioOption = .AUTO_REJECT_MANUALLY
    
    func prefItemClick(position:Int){
        self.activePrefPosition = self.activePrefPosition == position ? -1 : position
    }
    
    var body: some View {
        BaseGradientView()
            .edgesIgnoringSafeArea(.all)
            .overlay(
                VStack(alignment: .center, spacing: 12){
                    DeviceInfoCardCollapsedWithRename(
                        connectionState: vm.connectionState,
                        deviceInfo: vm.deviceInfo ,
                        battery: vm.batteryLevel,
                        renameMode: $vm.renameToggle, rename: {vm.rename()} , newName: $vm.newDeviceName)
                    
                    
                    ScrollView(.vertical){
                        VStack(alignment: .leading, spacing: 0){
                            // Whatsapp
                            PreferenceItem(position: 1, activeItemPosition: $activePrefPosition, onClick: prefItemClick(position:), bluArmorAppPreference:.init(icon: "pref.whatsapp", title: "WhatsApp", description: "Announce whatsapp messages")){
                                
                                SwitchWithLabel(switchState: $vm.whatsAppAutoRead, title: "WhatsApp Auto read", description: "Auto read whatsapp messages")
                            }
                            
                            // Phone call
                            PreferenceItem(position: 2, activeItemPosition: $activePrefPosition, onClick: prefItemClick(position:), bluArmorAppPreference:.init(icon: "pref.phone", title: "Phone Call", description: "Set your call-answer/reject preferences") ){
                                
                                
                                VStack(alignment: .leading, spacing: 0){
                                    PrefenceBodyLabel(title: "Auto Accept incoming calls", description: nil)
                                    
                                    RadioButtonWithLabel(active: $vm.phoneAutoAnswerAll,
                                                         label: "Accept all incoming calls",
                                                         onClick: { vm.phoneAutoAnswerChange(phoneCallAutoAnswerRadioOption: .AUTO_ANSWER_ALL) })
                                    
                                    RadioButtonWithLabel(active: $vm.phoneAutoAnswerKnown,
                                                         label: "Accept call from known contacts",
                                                         onClick: { vm.phoneAutoAnswerChange(phoneCallAutoAnswerRadioOption: .AUTO_ANSWER_KNOWN)})
                                    
                                    RadioButtonWithLabel(active: $vm.phoneAutoAnswerManual,
                                                         label: "Accept call with the button click",
                                                         onClick: {vm.phoneAutoAnswerChange(phoneCallAutoAnswerRadioOption: .AUTO_ANSWER_MANUALLY)})
                                }.onReceive(Just(vm.phoneAutoAnswer), perform:{
                                    phoneAutoAnswer = $0
                                })
                                
                                
                                VStack(alignment: .leading, spacing: 0){
                                    PrefenceBodyLabel(title: "Auto Reject incoming calls", description: nil)
                                    
                                    RadioButtonWithLabel(active: $vm.phoneAutoRejectAll,
                                                         label: "Reject all incoming calls",
                                                         onClick: { vm.phoneAutoRejectChange(phoneCallAutoRejectRadioOption: .AUTO_REJECT_ALL) })
                                    RadioButtonWithLabel(active: $vm.phoneAutoRejectUnknown,
                                                         label: "Reject call from unknown contacts",
                                                         onClick: { vm.phoneAutoRejectChange(phoneCallAutoRejectRadioOption: .AUTO_REJECT_UNKNOWN) })
                                    RadioButtonWithLabel(active: $vm.phoneAutoRejectManual,
                                                         label: "Reject call with the button click",
                                                         onClick: { vm.phoneAutoRejectChange(phoneCallAutoRejectRadioOption: .AUTO_REJECT_MANUALLY) })
                                }
                                
                            }
                            
                            // Audio
                            PreferenceItem(position: 3, activeItemPosition: $activePrefPosition, onClick: prefItemClick(position:), bluArmorAppPreference:.init(icon: "pref.audio", title: "Audio Control", description: "Adjust audio profile") ){
                                
                                
                                
                                VStack(alignment: .leading, spacing: 0){
                                    PrefenceBodyLabel(title: "Volume Boost", description: nil)
                                    
                                    RadioButtonWithLabel(active: $vm.volumeBoostEasyOnEar,
                                                         label: "Easy on the ears",
                                                         onClick: { vm.volumeBoostChange(volumeBoostRadioOption: .EASE_ON_EARS) })
                                    RadioButtonWithLabel(active: $vm.volumeBoostBalanced,
                                                         label: "Balanced sound",
                                                         onClick: { vm.volumeBoostChange(volumeBoostRadioOption: .BALANCED) })
                                    RadioButtonWithLabel(active: $vm.volumeBoostHighOutput,
                                                         label: "High output",
                                                         onClick: { vm.volumeBoostChange(volumeBoostRadioOption: .HIGH_OUTPUT) })
                                }
                                
                                VStack(alignment: .leading, spacing: 0){
                                    PrefenceBodyLabel(title: "Equalizer", description: nil)
                                    
                                    RadioButtonWithLabel(active: $vm.equalizerBoostNormal,
                                                         label: "Regular",
                                                         onClick: { vm.equalizerBoostChange(equalizerBoostRadioOption: .REGULAR) })
                                    RadioButtonWithLabel(active: $vm.equalizerBassBoost,
                                                         label: "Bass Boost",
                                                         onClick: { vm.equalizerBoostChange(equalizerBoostRadioOption: .BASS_BOOST) })
                                }
                                
                                if let connectionState = vm.connectionState, case ConnectionState.DeviceReady(let bluArmorDevice) = connectionState{
                                VStack(alignment: .leading, spacing: 0){
                                    PrefenceBodyLabel(title: "Voice Prompt", description: nil)
                                    
                                    RadioButtonWithLabel(active: $vm.voicePromptLevel2,
                                                         label: "All the voice prompt",
                                                         onClick: { vm.changeVoicePromptLevel(voicePromptLevel: .ALL)  })
                                    RadioButtonWithLabel(active: $vm.voicePromptLevel1,
                                                         label: "Only important voice prompt",
                                                         onClick: { vm.changeVoicePromptLevel(voicePromptLevel: .IMPORTANT)  })
                                }}
                                
                            }
                            
                            // RIDEGRID
                            PreferenceItem(position: 4, activeItemPosition: $activePrefPosition, onClick: prefItemClick(position:), bluArmorAppPreference:.init(icon: "pref.ridegrid", title: "RideGrid", description: "Setup RIDEGRID preferences")){
                                
                                SwitchWithLabel(switchState: $vm.audioOverlay, title: "Music Overlay", description: "Lowers the music volume when RIDEGRID call is active")
                                    .frame(maxWidth: .infinity, minHeight: 28,alignment: .leading)
                                
                                PrefenceBodyLabelWithValue(title: "Music Overlay Level", value: String(Int(vm.audioOverlayLevel)), description: nil)
                                
                                BigSliderView(
                                    maxLevel: 7,
                                    level:vm.audioOverlayLevel,
                                    onProgressCallBack: {level in
                                        vm.audioOverlayLevelChange(value: Float(level))
                                    })
                                .padding(.horizontal, 20)
                                .frame(maxWidth: .infinity, maxHeight: 28,alignment: .leading)
                            }
                            
                            // Second Phone
                            if let connectionState = vm.connectionState, case ConnectionState.DeviceReady(let bluArmorDevice) = connectionState{
                                PreferenceItem(position: 5, activeItemPosition: $activePrefPosition, onClick: prefItemClick(position:), bluArmorAppPreference:.init(icon: "pref.phone", title: "Second Phone Pairing", description: "Pair second phone to answer calls") ){
                                    
                                    SecondPhonePreferenceBody(deviceInfo: $vm.deviceInfo, secondPhoneState: $vm.secondPhoneState,
                                                              startArvertising: {vm.startSecondPhoneAdvertising()}, stopArvertising: {vm.stopSecondPhoneAdvertising()}, disconnectPhone: {vm.disconnectSecondPhone()})
                                }
                            }
                            
                            Spacer(minLength: 16)
                            
                            if let version =  Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String {
                                
                                Text("App Version: \(version)".uppercased())   .font(.system(.caption))
                                    .fontWeight(.semibold)
                                    .foregroundColor(ColorTextPrimaryAccent)
                                    .padding(.horizontal, 28)
                                    .frame(maxWidth: .infinity,alignment: .leading)
                            }
                            
                            if let connectionState = vm.connectionState, case ConnectionState.DeviceReady(let bluArmorDevice) = connectionState{
                                if let version = vm.deviceInfo?.firmwareApplicationVersion{
                                    Text("Fw Version: \(version)".uppercased())
                                        .font(.system(.caption))
                                        .fontWeight(.semibold)
                                        .foregroundColor(ColorTextPrimaryAccent)
                                        .padding(.horizontal, 28)
                                        .frame(maxWidth: .infinity,alignment: .leading)
                                }
                                
                                if let version = vm.deviceInfo?.rideGridApplicationVersion{
                                    Text("RIDEGRID Version: \(version)".uppercased())   .font(.system(.caption))
                                        .fontWeight(.semibold)
                                        .foregroundColor(ColorTextPrimaryAccent)
                                        .padding(.horizontal, 28)
                                        .frame(maxWidth: .infinity,alignment: .leading)
                                }
                                
                                if let version = vm.deviceInfo?.srNo{
                                    Text("Sr No: \(version)".uppercased())   .font(.system(.caption))
                                        .fontWeight(.semibold)
                                        .foregroundColor(ColorTextPrimaryAccent)
                                        .padding(.horizontal, 28)
                                        .frame(maxWidth: .infinity,alignment: .leading)
                                }
                            }
                        }
                    }
                    
                    Spacer(minLength: 0).frame( width:0, height: 0)
                    
                }.frame( alignment: .center )
                    .onAppear(perform: {
                        vm.refreshPreferences()
                    })
                
            )
    }
}

struct PreferenceView: View {
    @ObservedObject var vm:BluArmorAppViewModel
    
    var body: some View {
        if #available(iOS 14.0, *) {
            PreferenceContentView(vm:vm)
                .navigationBarTitleDisplayMode(.inline)
                .toolbar{
                    ToolbarItem(placement: .principal) {
                        
                        Text("Settings".uppercased())
                            .font(.system(.footnote))
                            .fontWeight(.bold)
                            .foregroundColor(ColorTextPrimaryAccent)
                    }
                    
                    ToolbarItem(placement: .navigationBarLeading) {
                        Button(action:{
                            self.vm.popBack()
                        }){
                            Image("back.nav")
                                .resizable()
                                .scaledToFit()
                                .frame( height: 44, alignment: .center)
                        }}
                }
        }
        else{
            NavigationView{
                PreferenceContentView(vm:vm)
                    .navigationBarTitle("Settings", displayMode: .inline)
                    .navigationBarItems(leading: Button(action:{
                        self.vm.popBack()
                    }){
                        Image("back.nav")
                            .renderingMode(.original)
                            .resizable()
                            .scaledToFit()
                            .frame( height: 44, alignment: .center)
                    })
            }
        }
    }
}


struct PreferenceItem<Content: View>: View {
    let position:Int
    @Binding var activeItemPosition:Int
    let onClick:(Int)->Void
    let bluArmorAppPreference:BluArmorAppPreference
    
    @ViewBuilder let bodyContent:Content
    
    var body: some View {
        VStack(spacing: 0){
            PreferenceHeader(onClick: { self.onClick(position)}, bluArmorAppPreference: self.bluArmorAppPreference, expanded: activeItemPosition == position)
            
            if activeItemPosition == position{
                VStack(alignment: .leading, spacing: 1){ bodyContent }
            }
        }
        .frame(maxWidth:.infinity)
    }
}



struct SecondPhonePreferenceBody: View {
    @Binding var deviceInfo:DeviceDetails?
    @Binding var secondPhoneState:SecondPhoneState?
    let startArvertising: ()-> Void
    let stopArvertising: ()-> Void
    let disconnectPhone: ()-> Void
    
    var body: some View {
        VStack(spacing: 0){
            switch secondPhoneState ?? SecondPhoneState.DISCONNECTED{
            case SecondPhoneState.ADVERTISING:             PrefenceBodyLabel(title: "Second phone is ready to connect ", description: "On your second phone connect with \(deviceInfo?.deviceName ?? "your device")")
            case SecondPhoneState.CONNECTED:             PrefenceBodyLabel(title: "Second phone is connected", description: "In order to user RIDELYNK, second phone needs to be disconnected")
            case SecondPhoneState.DISCONNECTED:             PrefenceBodyLabel(title: nil, description: "Start Advertising to pair the second phone")
            case SecondPhoneState.SERVICE_DISCOVERY:             PrefenceBodyLabel(title: nil, description: "Start Advertising to pair the second phone")
            }
            
            Button(action: {
                switch secondPhoneState ?? SecondPhoneState.DISCONNECTED{
                case SecondPhoneState.ADVERTISING:
                    stopArvertising()
                case SecondPhoneState.CONNECTED:
                    disconnectPhone()
                case SecondPhoneState.DISCONNECTED:
                    startArvertising()
                case SecondPhoneState.SERVICE_DISCOVERY:
                    startArvertising()
                }
            }, label: {
                
                switch secondPhoneState ?? SecondPhoneState.DISCONNECTED{
                case SecondPhoneState.ADVERTISING:
                    Text("Dismiss Second phone pairing".uppercased()).font(.system(.footnote)).fontWeight(.semibold).foregroundColor(ColorShark).padding(.vertical, 12).padding(.horizontal , 34)
                case SecondPhoneState.CONNECTED:
                    Text("Disconnect Second Phone".uppercased()).font(.system(.footnote)).fontWeight(.semibold).foregroundColor(ColorShark).padding(.vertical, 12).padding(.horizontal , 34)
                case SecondPhoneState.DISCONNECTED:
                    Text("Connect Second Phone".uppercased()).font(.system(.footnote)).fontWeight(.semibold).foregroundColor(ColorShark).padding(.vertical, 12).padding(.horizontal , 34)
                case SecondPhoneState.SERVICE_DISCOVERY:
                    Text("Connect Second Phone".uppercased()).font(.system(.footnote)).fontWeight(.semibold).foregroundColor(ColorShark).padding(.vertical, 12).padding(.horizontal , 34)
                }
                
                
            })
            .background(Color.white)
            .cornerRadius(38.5)
            .padding()
            .frame(
                maxWidth: .infinity,
                maxHeight: .infinity,
                alignment: .bottomTrailing
            )
            .shadow(color: Color.black.opacity(0.3),
                    radius: 3,
                    x: 3,
                    y: 3)
        }
        .frame(maxWidth:.infinity)
    }
}

struct PreferenceHeader: View {
    let onClick:()->Void
    let bluArmorAppPreference:BluArmorAppPreference
    let expanded:Bool
    
    var body: some View {
        Button(action: self.onClick){
            HStack(alignment: .center, spacing: 12){
                Image(bluArmorAppPreference.icon)
                    .renderingMode(.original)
                    .resizable()
                    .aspectRatio( contentMode:.fit)
                    .scaledToFit()
                    .frame(width: 48, alignment: .leading)
                
                VStack(alignment:.leading, spacing:0){
                    Text(bluArmorAppPreference.title)
                        .font(.system(.callout))
                        .fontWeight(.semibold)
                        .foregroundColor(ColorTextPrimary)
                        .frame(maxWidth: .infinity,alignment: .leading)
                    
                    Text(bluArmorAppPreference.description)
                        .font(.system(.footnote))
                        .fontWeight(.regular)
                        .foregroundColor(ColorTextPrimaryAccent)
                        .frame(maxWidth: .infinity,alignment: .leading)
                        .multilineTextAlignment(.leading)
                }
                
                Image(systemName: "chevron.up")
                    .resizable()
                    .aspectRatio( contentMode:.fit)
                    .scaledToFit()
                    .foregroundColor(.white)
                    .frame(width: 7,height: 7, alignment: .leading)
                    .rotationEffect(.degrees(expanded ? 0.0 : 180.0))
            }
        }
        .padding(16)
        .frame(maxWidth:.infinity)
    }
}

struct SwitchWithLabel: View {
    @Binding var switchState:Bool
    let title:String?
    let description:String?
    
    
    var body: some View {
        VStack(spacing: 0){
            if let title = title{
                Toggle(title, isOn: $switchState)
                    .font(.system(.callout).weight(.semibold))
                    .foregroundColor(ColorTextPrimary)
                    .frame(maxWidth: .infinity,alignment: .leading)
            }
            if let description = description{
                Text(description)
                    .font(.system(.callout))
                    .fontWeight(.regular)
                    .foregroundColor(ColorTextPrimaryAccent)
                    .frame(maxWidth: .infinity,alignment: .leading)
            }
        }
        .padding(.vertical, 8)
        .padding(.horizontal, 16)
        .background(Color(hex: 0x11FFFFFF))
        .frame(maxWidth:.infinity)
    }
    
}

struct PrefenceBodyLabel: View {
    let title:String?
    let description:String?
    
    var body: some View {
        VStack(spacing: 0){
            if let title = title{
                Text(title)
                    .font(.system(.callout).weight(.semibold))
                    .foregroundColor(ColorTextPrimary)
                    .frame(maxWidth: .infinity,alignment: .leading)
            }
            if let description = description{
                Text(description)
                    .font(.system(.callout))
                    .fontWeight(.regular)
                    .foregroundColor(ColorTextPrimaryAccent)
                    .frame(maxWidth: .infinity,alignment: .leading)
            }
        }
        .padding(.vertical, 8)
        .padding(.horizontal, 16)
        .background(Color(hex: 0x11FFFFFF))
        .frame(maxWidth:.infinity)
    }
}

struct PrefenceBodyLabelWithValue: View {
    let title:String?
    let value:String?
    let description:String?
    
    var body: some View {
        VStack(spacing: 0){
            if let title = title{
                HStack{
                    Text(title)
                        .font(.system(.callout).weight(.semibold))
                        .foregroundColor(ColorTextPrimary)
                    if let value = value{
                        Spacer()
                        Text(value)
                            .font(.system(.callout).weight(.bold))
                            .foregroundColor(ColorTextPrimary)
                    }
                }
                .frame(maxWidth: .infinity,alignment: .leading)
            }
            if let description = description{
                Text(description)
                    .font(.system(.callout))
                    .fontWeight(.regular)
                    .foregroundColor(ColorTextPrimaryAccent)
                    .frame(maxWidth: .infinity,alignment: .leading)
            }
        }
        .padding(.vertical, 8)
        .padding(.horizontal, 16)
        .background(Color(hex: 0x11FFFFFF))
        .frame(maxWidth:.infinity)
    }
}

struct RadioButtonWithLabel: View {
    @Binding var active:Bool
    let label:String
    let onClick:()->Void
    
    var body: some View {
        Button(action: onClick ){
            HStack(alignment: .center, spacing: 8){
                
                Image("bubble.square")
                    .renderingMode(.template)
                    .resizable()
                    .scaledToFit()
                    .frame( height: 14, alignment: .center)
                    .foregroundColor(active ? ColorPositive : Color(hex: 0x27FFFFFF))
                
                Text(label)
                    .font(.system(.callout))
                    .fontWeight(.regular)
                    .foregroundColor(active ? ColorTextPrimary :ColorTextPrimaryAccent)
                    .frame(maxWidth: .infinity,alignment: .leading)
            }
        }.padding(.vertical, 8)
            .padding(.horizontal, 16)
            .background(Color(hex: 0x11FFFFFF))
            .frame(maxWidth:.infinity)
    }
}

struct BluArmorAppPreference{
    let icon: String
    let title: String
    let description: String
}

//struct PreferenceView_Previews: PreviewProvider {
//    static var previews: some View {
//        PreferenceView()
//    }
//}


struct DeviceInfoCardCollapsedWithRename: View {
    var connectionState:ConnectionState?
    var deviceInfo:DeviceDetails?
    var battery:BatteryLevel?
    @Binding var renameMode:Bool
    
    let rename: () -> Void
    @Binding var newName:String
    
    
    var body: some View {
        ZStack(alignment: .topLeading){
            LinearGradient(
                gradient: Gradient(colors: [Color(hex: 0xFF2E5A80),Color(hex: 0xFF254C6D),Color(hex: 0xFF0C2941)]), startPoint: .leading, endPoint: .trailing)
            .cornerRadius(8)
            .padding(.top ,18)
            
            Image("Sx20Small")
                .resizable()
                .aspectRatio( contentMode:.fit)
                .scaledToFit()
                .frame(width: 78, alignment: .leading)
            
            VStack(alignment: .leading, spacing: 10){
                HStack(alignment: .top, spacing: 4){
                    VStack(alignment: .leading){
                        if let connectionState = connectionState, case ConnectionState.DeviceReady(let bluArmorDevice ) = connectionState {
                            Text(deviceInfo?.deviceName ?? "Disconnected")
                                .font(.system(.subheadline)).fontWeight(.semibold)
                                .foregroundColor(ColorTextPrimary)
                        }else{
                            Text("Disconnected")
                                .font(.system(.subheadline)).fontWeight(.semibold)
                                .foregroundColor(ColorTextPrimary)
                        }
                        
                        if let connectionState = connectionState, case ConnectionState.DeviceReady(let bluArmorDevice) = connectionState, let modelLabel = deviceInfo?.model.label{
                            Text(modelLabel)
                                .font(.system(.caption))
                                .fontWeight(.regular)
                                .foregroundColor(ColorTextPrimaryAccent)
                        }
                        
                    }
                    .frame(
                        maxWidth: .infinity,
                        maxHeight: .infinity,
                        alignment: .leading
                    )
                    if let connectionState = connectionState, case ConnectionState.DeviceReady(let bluArmorDevice) = connectionState{
                        Button(action: {renameMode = !renameMode} ){
                            HStack(alignment: .center, spacing: 8){
                                Image("edit")
                                    .renderingMode(.template)
                                    .resizable()
                                    .scaledToFit()
                                    .frame( height: 24, alignment: .center)
                                    .foregroundColor( Color(hex: 0xFFFFFFFF))
                                
                            }
                        }
                        .frame(maxWidth:.infinity)
                        
                        if let batt = battery{
                            Text("\(batt.level)%").font(.system(.caption)).fontWeight(.semibold)
                                .foregroundColor(ColorTextPrimary)
                        }
                    }
                    
                }
                .padding(.leading, 78)
                .padding(.trailing, 16)
                
                if renameMode {
                    TextFieldView(text: $newName, label: "New Name", suggestionText: "", showCharacterCount: true, maxCharacterCount: 10)
                        .padding(.horizontal , 34)
                    
                    HStack{
                        
                        Button(action: {
                            renameMode = false
                        }, label: {
                            Text("cancel".uppercased()).font(.system(.footnote)).fontWeight(.semibold).foregroundColor(ColorShark).padding(.vertical, 12).padding(.horizontal , 34)
                            
                        })
                        .background(Color.white)
                        .cornerRadius(38.5)
                        .padding()
                        .frame(
                            maxWidth: .infinity,
                            maxHeight: .infinity,
                            alignment: .bottomTrailing
                        )
                        .shadow(color: Color.black.opacity(0.3),
                                radius: 3,
                                x: 3,
                                y: 3)
                        
                        Spacer()
                            .frame(width: 32)
                        
                        Button(action: {
                            rename()
                            renameMode = false
                            
                        }, label: {
                            Text("Rename".uppercased()).font(.system(.footnote)).fontWeight(.semibold).foregroundColor(ColorShark).padding(.vertical, 12).padding(.horizontal , 34)
                            
                        })
                        .background(Color.white)
                        .cornerRadius(38.5)
                        .padding()
                        .frame(
                            maxWidth: .infinity,
                            maxHeight: .infinity,
                            alignment: .bottomTrailing
                        )
                        .shadow(color: Color.black.opacity(0.3),
                                radius: 3,
                                x: 3,
                                y: 3)
                    }
                    
                }
            }
            .padding(.top ,18)
            .padding(.vertical, 10)
        }
        .padding(.horizontal, 16)
        .fixedSize(horizontal: false, vertical: true)
    }
}
