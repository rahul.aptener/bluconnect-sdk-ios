//
//  BuddyListView.swift
//  BluArmor-App
//
//  Created by Rahul Gaur on 01/02/22.
//

import Foundation
import SwiftUI
import BluConnect

struct BuddyListContentView<VM:BuddyListViewModel>: View {
    @ObservedObject var vm:VM
    
    var body: some View {
        BaseGradientView()
            .edgesIgnoringSafeArea(.all)
        
        
            .overlay(
                VStack(alignment: .center, spacing: 12){
                    
                    DeviceInfoCardCollapsed(
                        deviceInfo: vm.deviceInfo ,
                        battery: vm.batteryLevel
                    )
                    
                    HStack(alignment: .center, spacing: 12){
                        
                        Text("Buddies".uppercased())   .font(.system(.caption))
                            .fontWeight(.semibold)
                            .foregroundColor(ColorTextPrimary)
                            .padding(.leading, 12)
                        
                        Spacer()
                        
                        Button(action: vm.navigateToAddBuddiesInviteView){
                            Text("Add Buddies".uppercased())   .font(.system(.caption))
                                .fontWeight(.semibold)
                                .foregroundColor(ColorTextPrimaryAccent)
                                .padding(.horizontal,10)
                                .padding(.vertical,4)
                                .background(ColorShark)
                                .cornerRadius(100)
                        }
                        
                    }.padding(.horizontal, 16)
                        .frame(maxWidth: .infinity)
                    
                    
                    
                    //                    ScrollView(.vertical) {
                    //                                        Text("\( String(describing:Array(vm.buddyList).map{ $0.name }))")
                    
                    if vm.buddyList.count > 0{
                        List{
                            ForEach(vm.buddyList.unique(), id: \.macAddress){ buddy in
                                DiscoveredDeviceView(
                                    label: buddy.name,
                                    action:{ vm.rideLynkAction(action: .Connect(device: buddy)) }
                                )
                            }
                        }
                    }else { Spacer() }
                    
                    //                        LazyVStack(alignment: .center, spacing: 12){
                    //                            ForEach(Array(vm.buddyList), id: \.self){ buddy in
                    //                                DiscoveredDeviceView(
                    //                                    label: buddy.name,
                    //                                    action:{ /** vm.rideLynkAction(action: .Connect(device: device))*/ }
                    //                                )
                    //                                    .listRowSeparator(.hidden)
                    //                                    .listRowInsets(EdgeInsets(top:0, leading:0, bottom: 8, trailing: 0))
                    //                            }
                    //                        }
                    //                    }
                    
                    HStack(){
                        
                        Text("Speed Dial".uppercased())   .font(.system(.caption))
                            .fontWeight(.semibold)
                            .foregroundColor(ColorTextPrimary)
                            .padding(.leading, 12)
                        Spacer()
                        
                        Button(action: vm.navigateToSpeedDialEditView){
                            Text("Edit".uppercased())   .font(.system(.caption))
                                .fontWeight(.semibold)
                                .foregroundColor(ColorTextPrimaryAccent)
                                .padding(.horizontal,10)
                                .padding(.vertical,4)
                                .background(ColorShark)
                                .cornerRadius(100)
                        }
                    }
                    .padding(.horizontal, 16)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    
                    SpeedDialContactsView(contacts: $vm.speedDialDevices, action: {
                        vm.connectSpeedDialDevice(speedDialDevice: $0)
                    })
                    
                    // For the visibility of centent under FAB
                    Spacer(minLength: 0).frame( width:0, height: 0)
                    
                }.frame( alignment: .center )
                
            )
            .onAppear{
                vm.refreshBuddyList()
            }
    }
}

struct BuddyListView: View {
    @ObservedObject var vm:BluArmorAppViewModel
    
    var body: some View {
        if #available(iOS 14.0, *) {
            BuddyListContentView(vm:vm)
                .navigationBarTitleDisplayMode(.inline)
                .toolbar{
                    ToolbarItem(placement: .navigationBarLeading) {
                        Button(action:{
                            self.vm.popBack()
                        }){
                            Image("back.nav")
                                .resizable()
                                .scaledToFit()
                                .frame( height: 44, alignment: .center)
                        }}
                }
        }
        else{
            NavigationView{
                BuddyListContentView(vm:vm)
                    .navigationBarTitle("", displayMode: .inline)
                    .navigationBarItems(leading:  Button(action:{
                        self.vm.popBack()
                    }){
                        Image("back.nav")
                            .renderingMode(.original)
                            .resizable()
                            .scaledToFit()
                            .frame( height: 44, alignment: .center)
                    }
                    )
            }
        }
    }
}

//
//struct BuddyListView_Previews: PreviewProvider {
//    static var previews: some View {
//        BuddyListView(vm: BluArmorApp.bluArmorContainer.bluArmorAppViewModel)
//    }
//}

extension Sequence where Iterator.Element: Hashable {
    func unique() -> [Iterator.Element] {
        var seen: [Iterator.Element: Bool] = [:]
        return self.filter { seen.updateValue(true, forKey: $0) == nil }
    }
}
