//
//  SpeedDialEditView.swift
//  BluArmor-App
//
//  Created by Rahul Gaur on 05/02/22.
//

import SwiftUI
import BluConnect

protocol SpeedDialEditViewModel:ObservableObject{
    var buddyList: Set<Buddy> { get }
    var speedDialDevices:Array<Buddy?> { get }
    var newSpeedDialList:Set<Buddy> { get }
    
    func refreshSpeedDialList()
    func addBuddyToSpeedDial(buddy:Buddy)
    func removeSpeedDialDevice(speedDialDevice:Buddy)
    func saveSpeedDialList()
    func popBack()
}

struct SpeedDialEditContentView<VM:SpeedDialEditViewModel>: View {
    @ObservedObject var vm: VM
    
    var body: some View {
        BaseGradientView()
            .edgesIgnoringSafeArea(.all)
            .overlay(
                VStack(alignment: .center, spacing: 12){
                    
                    
                    HStack(alignment: .center, spacing: 12){
                        
                        Text("Speed Dial List".uppercased())
                            .font(.system(.caption))
                            .fontWeight(.semibold)
                            .foregroundColor(ColorTextPrimary)
                        
                        Spacer()
                        
                        Text("0/5".uppercased())
                            .font(.system(.caption))
                            .fontWeight(.semibold)
                            .foregroundColor(ColorTextPrimary)
                    }.padding(.horizontal, 16+12)
                        .frame(maxWidth: .infinity)
                    
                    List{
                        ForEach(Array(vm.newSpeedDialList), id: \.self){ device in
                            BuddyDeviceView(
                                label: device.name,
                                action:{ vm.removeSpeedDialDevice(speedDialDevice: device) },
                                positiveAction: false
                            )
                        }
                    }
                    
//                    LazyVStack(alignment: .center, spacing: 12){
//                        ForEach(Array(vm.newSpeedDialList), id: \.self){ device in
//
//                            BuddyDeviceView(
//                                label: device.name,
//                                action:{ vm.removeSpeedDialDevice(speedDialDevice: device) },
//                                positiveAction: false
//                            )
//                        }
//                    }
                    
                    
                    HStack(alignment: .center, spacing: 12){
                        Button(action:{ vm.popBack() }){
                            Text("Discard".uppercased())
                                .font(.system(.footnote))
                                .fontWeight(.semibold)
                                .foregroundColor(.white)
                                .padding(.vertical, 10)
                                .padding(.horizontal, 20)
                                .overlay(
                                    RoundedRectangle(cornerRadius: 100)
                                        .stroke(.white, lineWidth: 2)
                                )
                        }
                        
                        Button(action:{ vm.saveSpeedDialList() }){
                            Text("Update List".uppercased())
                                .font(.system(.footnote))
                                .fontWeight(.semibold)
                                .foregroundColor(ColorShark)
                                .padding(.vertical, 10)
                                .padding(.horizontal, 20)
                                .frame(maxWidth: .infinity)
                                .background(Color.white)
                                .cornerRadius(100)
                        }
                        
                    }
                    .padding(.horizontal, 16)
                    
                    
                    HStack(){
                        Text("Buddy List".uppercased())
                            .font(.system(.caption))
                            .fontWeight(.semibold)
                            .foregroundColor(ColorTextPrimary)
                            .padding(.leading, 12)
                        Spacer()
                    }
                    .padding(.horizontal, 16)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    
//                    ScrollView(.vertical) {
                        List{
                            ForEach(Array(vm.buddyList.filter({ buddy in
                                buddy.model != .NO_DEVICE
                            })), id: \.self){ buddy in
                                BuddyDeviceView(
                                    label: buddy.name,
                                    action:{ vm.addBuddyToSpeedDial(buddy: buddy) },
                                    positiveAction: true,
                                    enabled: vm.newSpeedDialList.count < 5
                                )
                            }
                        }
//                        LazyVStack(alignment: .center, spacing: 12){
//                            ForEach(Array(vm.buddyList), id: \.self){ buddy in
//                                BuddyDeviceView(
//                                    label: buddy.name,
//                                    action:{ vm.addBuddyToSpeedDial(buddy: buddy) },
//                                    positiveAction: true,
//                                    enabled: vm.newSpeedDialList.count < 2
//                                )
//                                    .listRowSeparator(.hidden)
//                                    .listRowInsets(EdgeInsets(top:0, leading:0, bottom: 8, trailing: 0))
//                            }
//                        }
//                    }
                    // For the visibility of centent under FAB
                    Spacer(minLength: 0).frame( width:0, height: 0)
                    
                }.frame( alignment: .center )
            )
    }
}

struct SpeedDialEditView: View {
    @ObservedObject var vm:BluArmorAppViewModel
    
    var body: some View {
        if #available(iOS 14.0, *) {
            SpeedDialEditContentView(vm:vm)
                        .navigationBarTitleDisplayMode(.inline)
                        .toolbar{
                            ToolbarItem(placement: .navigationBarLeading) {
                                Button(action:{
                                    self.vm.popBack()
                                }){
                                    Image("back.nav")
                                        .resizable()
                                        .scaledToFit()
                                        .frame( height: 44, alignment: .center)
                                }}
                        }
        }
        else{
            NavigationView{
                SpeedDialEditContentView(vm:vm).navigationBarTitle("", displayMode: .inline)
                    .navigationBarItems(leading: Button(action:{
                        self.vm.popBack()
                    }){
                        Image("back.nav")
                            .renderingMode(.original)
                            .resizable()
                            .scaledToFit()
                            .frame( height: 44, alignment: .center)
                    })
       
            }
        }
    }
}


struct BuddyDeviceView: View {
    let label:String
    let action:()->Void
    let positiveAction:Bool
    var enabled:Bool = true

    var body: some View {
        HStack(alignment: .center, spacing: 12){
            
            Image("helmet")
                .resizable()
                .scaledToFit()
                .frame( width: 24,height: 24,  alignment: .center)
            
            Text(label)
                .font(.system(.subheadline))
                .fontWeight(.semibold)
                .foregroundColor(ColorTextPrimaryAccent)
                .padding(.leading, 0)
                .frame(maxWidth: .infinity,alignment: .leading)
            
            if enabled{
            Button(action:self.action){
                Image(positiveAction ? "add.colored" : "remove.colored")
                    .resizable()
                    .aspectRatio( contentMode:.fit)
                    .scaledToFit()
                    .padding(.horizontal, 6)
                    .frame( width: 48,height: 48, alignment: .center)
            }}
            else {
                Spacer()
                    .padding(.horizontal, 6)
                    .frame( width: 48,height: 48, alignment: .center)
            }
        }
        .padding(.leading, 12)
        .background(Color(0x1f1f1f))
        .cornerRadius(10)
        .padding(.horizontal, 16)
        
        //
    }
}

//
//struct SpeedDialEditView_Previews: PreviewProvider {
//    static var previews: some View {
//        SpeedDialEditView(vm: BluArmorApp.bluArmorContainer.bluArmorAppViewModel)
//    }
//}
