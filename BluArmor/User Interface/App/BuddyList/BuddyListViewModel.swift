//
//  BuddyListViewModel.swift
//  BluArmor
//
//  Created by Rahul Gaur on 15/03/22.
//

import Foundation
import BluConnect

protocol BuddyListViewModel:ObservableObject{
    var buddyList: Set<Buddy> { get }
    var speedDialDevices: Array<Buddy?> { get set }
    var rideLynkState:RideLynkState? { get }
    
    var deviceInfo: DeviceDetails? { get }
    var batteryLevel:BatteryLevel? { get }
    
    func popBack()
    func navigateToSpeedDialEditView()
    func navigateToAddBuddiesInviteView()
    func refreshBuddyList()

    func rideLynkAction(action:RideLynkActions)
    func connectSpeedDialDevice(speedDialDevice: Buddy)
}
