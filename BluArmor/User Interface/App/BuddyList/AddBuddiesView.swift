//
//  AddBuddiesView.swift
//  BluArmor
//
//  Created by Rahul Gaur on 01/06/22.
//

import SwiftUI

struct AddBuddiesView: View {
    @ObservedObject var vm:BluArmorAppViewModel
    
    var body: some View {
        if #available(iOS 14.0, *) {
            BuddyShareInviteContentView(vm:vm)
                .navigationBarTitleDisplayMode(.inline)
                .toolbar{
                    ToolbarItem(placement: .navigationBarLeading) {
                        Button(action:{
                            self.vm.popBack()
                        }){
                            Image("back.nav")
                                .resizable()
                                .scaledToFit()
                                .frame( height: 44, alignment: .center)
                        }}
                }
        }
        else{
            NavigationView{
                BuddyShareInviteContentView(vm:vm)
                    .navigationBarTitle("", displayMode: .inline)
                    .navigationBarItems(leading: Button(action:{
                        self.vm.popBack()
                    }){
                        Image("back.nav")
                            .renderingMode(.original)
                            .resizable()
                            .scaledToFit()
                            .frame( height: 44, alignment: .center)
                    })
                
            }
        }
    }
}

struct BuddyShareInviteContentView: View {
    @ObservedObject var vm:BluArmorAppViewModel
    
    var body: some View {
        BaseGradientView()
            .edgesIgnoringSafeArea(.all)
            .overlay(
                VStack(alignment: .center, spacing: 12){
                    
                    QrCodeScannerView()
                        .found(r: self.vm.onBuddiedQrCodeDataScan)
                        .torchLight(isOn: self.vm.torchIsOn)
                        .interval(delay: self.vm.scanInterval)
                        .aspectRatio(1, contentMode: .fit)
                        .frame(maxWidth: .infinity)
                        .background(Color.white)
                        .cornerRadius(10)
                        .padding(.horizontal, 70)
                    
                    Text("Scan other rider’s QR code to join the group")   .font(.system(.caption))
                        .fontWeight(.semibold)
                        .foregroundColor(ColorTextPrimaryAccent)
                    
                    if let invitationData = vm.getBuddyInviteQrCodeData(), let qRCodeData = getQRCodeDate(text:invitationData), let uiImage = UIImage(data: qRCodeData), let activeBuddy = vm.getActiveBuddy() {
                        TextFieldViewDisabled(text:activeBuddy.name, label: "Buddy Name")
                            .padding(.horizontal, 16)
                            .padding(.vertical, 16)
                        
                        Image(uiImage:uiImage)
                            .resizable()
                            .frame(width: 200, height: 200)
                            .cornerRadius(8)
                    }
                    
                    Text("Scan Buddy’s QR code to join them").font(.system(.caption))
                        .fontWeight(.semibold)
                        .foregroundColor(ColorTextPrimaryAccent)
                    
                    Spacer()
                    
                    // For the visibility of centent under FAB
                    
                }.frame( alignment: .center )
                
            )
    }
    
    func getQRCodeDate(text: String) -> Data? {
        guard let filter = CIFilter(name: "CIQRCodeGenerator") else { return nil }
        let data = text.data(using: .ascii, allowLossyConversion: false)
        filter.setValue(data, forKey: "inputMessage")
        guard let ciimage = filter.outputImage else { return nil }
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        let scaledCIImage = ciimage.transformed(by: transform)
        let uiimage = UIImage(ciImage: scaledCIImage)
        return uiimage.pngData()!
    }
    
}




