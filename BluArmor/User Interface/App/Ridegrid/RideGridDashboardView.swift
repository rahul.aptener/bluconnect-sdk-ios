//
//  RideGridDashboardView.swift
//  BluArmor-App
//
//  Created by Rahul Gaur on 31/01/22.
//

import SwiftUI
import BluConnect

struct RideGridDashboardView: View {
    @ObservedObject var vm:BluArmorAppViewModel
    
    var body: some View {
        if #available(iOS 14.0, *) {
            RideGridDashboardContentView(vm:vm)
                .navigationBarTitleDisplayMode(.inline)
                .toolbar{
                    ToolbarItem(placement: .navigationBarLeading) {
                        Button(action:{
                            self.vm.popBack()
                        }){
                            Image("back.nav")
                                .resizable()
                                .scaledToFit()
                                .frame( height: 44, alignment: .center)
                        }}
                    
                    ToolbarItem(placement: .navigationBarTrailing) {
                        if let id = vm.inviteRideGridGroup?.id{
                            Button(action:{self.vm.navigateToInviteRiders(groupUniqueId:id)
                            }){
                                Text( "Invite")
                                    .font(.system(.caption))
                                    .fontWeight(.semibold)
                                    .foregroundColor(ColorShark)
                                    .padding(.horizontal, 12)
                                    .padding(.vertical, 6)
                                    .background(Color.white)
                                    .cornerRadius(100)
                            }}}
                }
        }
        else{
            NavigationView{
                RideGridDashboardContentView(vm:vm)
                    .navigationBarTitle("", displayMode: .inline)
                    .navigationBarItems(
                        leading:  Button(action:{
                            self.vm.popBack()
                        }){
                            Image("back.nav")
                                .renderingMode(.original)
                                .resizable()
                                .scaledToFit()
                                .frame( height: 44, alignment: .center)
                        },
                        trailing: Button(action:{
                            if let id = vm.inviteRideGridGroup?.id{
                                self.vm.navigateToInviteRiders(groupUniqueId:id)
                            }
                        }){
                            Text( "Invite")
                                .font(.system(.caption))
                                .fontWeight(.semibold)
                                .foregroundColor(ColorShark)
                                .padding(.horizontal, 12)
                                .padding(.vertical, 6)
                                .background(Color.white)
                                .cornerRadius(100)
                        })
            }
        }
    }
}


struct RideGridDashboardContentView: View {
    
    @ObservedObject var vm:BluArmorAppViewModel
    
    var body: some View {
        BaseGradientView()
            .edgesIgnoringSafeArea(.all)
        
            .overlay(
                ScrollView(.vertical){
                    VStack(alignment: .center, spacing: 12){
                        DeviceInfoCard(
                            deviceInfo:vm.deviceInfo,
                            device: vm.device ,
                            battery: vm.batteryLevel,
                            mediaPlayerState: vm.mediaPlayerState,
                            musicAction: vm.musicAction(action:),
                            dialerState: vm.dialerState,
                            dialerAction:vm.dialerAction(action:)
                        )
                        
                        if vm.rideGridState == RideGridState.OFF{
                            HStack(alignment: .center, spacing: 12){
                                Button(action: { vm.rideGridAction(action: .TurnOn)}){
                                    EdgeCardView(bottomStrokeColor: ColorPositive){
                                        VStack(alignment: .center, spacing: 0){
                                            Text( "Turn On".uppercased())
                                                .font(.system(.callout))
                                                .fontWeight(.semibold)
                                                .foregroundColor(ColorTextPrimary)
                                                .padding(.vertical, 0)
                                            
                                            Text( "RIDEGRID".uppercased())
                                                .font(.system(.footnote))
                                                .fontWeight(.semibold)
                                                .foregroundColor(ColorTextPrimaryAccent)
                                                .padding(.vertical, 0)
                                            
                                        }
                                        .frame(maxWidth: .infinity)
                                        .frame(height: 58)
                                    }
                                }
                            }
                            .padding(.horizontal, 16)
                            .padding(.vertical, 12)
                        }
                        
                        
                        if vm.rideGridState == RideGridState.INCOMING_CALL ||  vm.rideGridState == RideGridState.CONNECTED {
                            HStack(alignment: .center, spacing: 12){
                                Button(action: { vm.rideGridAction(action: .StartCall)}){
                                    EdgeCardView(bottomStrokeColor: ColorPositive){
                                        VStack(alignment: .center, spacing: 0){
                                            Text((vm.rideGridState == RideGridState.INCOMING_CALL ? "Join" : "Start" ).uppercased())
                                                .font(.system(.callout))
                                                .fontWeight(.semibold)
                                                .foregroundColor(ColorTextPrimary)
                                                .padding(.vertical, 0)
                                            
                                            Text( "Call".uppercased())
                                                .font(.system(.footnote))
                                                .fontWeight(.semibold)
                                                .foregroundColor(ColorTextPrimaryAccent)
                                                .padding(.vertical, 0)
                                        }
                                        .frame(maxWidth: .infinity)
                                        .frame(height: 58)
                                    }
                                }
                            }
                            .padding(.horizontal, 16)
                            .padding(.vertical, 12)
                        }
                        
                        if (vm.rideGridState == RideGridState.ACTIVE_CALL || vm.rideGridState == RideGridState.MUTED){
                            HStack(alignment: .center, spacing: 12){
                                Button(action: {
                                    vm.rideGridAction(action: .StopCall)
                                }){
                                    EdgeCardView(bottomStrokeColor: ColorNegative){
                                        VStack(alignment: .center, spacing: 0){
                                            
                                            Text( "End".uppercased())
                                                .font(.system(.callout))
                                                .fontWeight(.semibold)
                                                .foregroundColor(ColorTextPrimary)
                                                .padding(.vertical, 0)
                                            
                                            Text( "Call".uppercased())
                                                .font(.system(.footnote))
                                                .fontWeight(.semibold)
                                                .foregroundColor(ColorTextPrimaryAccent)
                                                .padding(.vertical, 0)
                                            
                                        }
                                        .frame(maxWidth: .infinity)
                                        .frame(height: 58)
                                        
                                    }
                                }
                                Button(action: {vm.rideGridAction(action: vm.rideGridState == RideGridState.MUTED ? RideGridActions.UnMute : RideGridActions.Mute)}){
                                    EdgeCardView(bottomStrokeColor: ColorPositive){
                                        VStack(alignment: .center, spacing: 0){
                                            Text( ( vm.rideGridState == RideGridState.MUTED ? "Unmute" : "Mute").uppercased())
                                                .font(.system(.callout))
                                                .fontWeight(.semibold)
                                                .foregroundColor(ColorTextPrimary)
                                                .padding(.vertical, 0)
                                            Text( "Call".uppercased())
                                                .font(.system(.footnote))
                                                .fontWeight(.semibold)
                                                .foregroundColor(ColorTextPrimaryAccent)
                                                .padding(.vertical, 0)
                                        }
                                        .frame(maxWidth: .infinity)
                                        .frame(height: 58)
                                        
                                    }
                                }
                            }
                            .padding(.horizontal, 16)
                            .padding(.vertical, 12)
                        }
                        
                        ZStack(alignment: .center){
                            
                            Circle()
                                .fill(Color(hex: 0xFF1A1A1A))
                                .frame(maxWidth:.infinity, alignment: .center )
                                .aspectRatio(1, contentMode: .fit)
                                .padding(32)
                            
                            
                            Circle()
                                .fill(Color(hex: 0xFF282828))
                                .frame(maxWidth:.infinity, alignment: .center )
                                .aspectRatio(1, contentMode: .fit)
                                .padding(72)
                            
                            
                            Circle()
                                .fill(Color(hex: 0xFF313131))
                                .frame(maxWidth:.infinity, alignment: .center )
                                .aspectRatio(1, contentMode: .fit)
                                .padding(112)
                            
                            
                            Image("iv_navigation")
                                .renderingMode(.original)
                                .resizable()
                                .scaledToFit()
                                .frame( height:32)
                            
                            ZStack(alignment: .center){
                                VStack(alignment: .center){
                                    VStack(alignment: .center){
                                        
                                        HStack(alignment: .center){
                                            Spacer()
                                            RideGridUserMiniCapsuleView(
                                                name: vm.rideGridRidersRadarMap["T12"]?.name,
                                                distance: vm.rideGridRidersRadarMap["T12"]?.distance
                                            )
                                            Spacer()
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["T10"]?.name, distance: vm.rideGridRidersRadarMap["T10"]?.distance)
                                            Spacer()
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["T11"]?.name, distance: vm.rideGridRidersRadarMap["T11"]?.distance)
                                            Spacer()
                                        }
                                        
                                        HStack(alignment: .center){
                                            
                                            Spacer()
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["T9"]?.name, distance: vm.rideGridRidersRadarMap["T9"]?.distance)
                                            Spacer()
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["T8"]?.name, distance: vm.rideGridRidersRadarMap["T8"]?.distance)
                                            Spacer()
                                        }
                                        
                                        HStack(alignment: .center){
                                            
                                            Spacer()
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["T7"]?.name, distance: vm.rideGridRidersRadarMap["T7"]?.distance)
                                            Spacer()
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["T5"]?.name, distance: vm.rideGridRidersRadarMap["T5"]?.distance)
                                            Spacer()
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["T6"]?.name, distance: vm.rideGridRidersRadarMap["T6"]?.distance)
                                            Spacer()
                                        }
                                        
                                        HStack(alignment: .center){
                                            Spacer()
                                            Spacer()
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["T3"]?.name, distance: vm.rideGridRidersRadarMap["T3"]?.distance)
                                            Spacer()
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["T4"]?.name, distance: vm.rideGridRidersRadarMap["T4"]?.distance)
                                            Spacer()
                                            Spacer()
                                        }
                                        
                                        HStack(alignment: .center){
                                            
                                            Spacer()
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["T14"]?.name, distance: vm.rideGridRidersRadarMap["T14"]?.distance)
                                            Spacer()
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["T2"]?.name, distance: vm.rideGridRidersRadarMap["T2"]?.distance)
                                            Spacer()
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["T13"]?.name, distance: vm.rideGridRidersRadarMap["T13"]?.distance)
                                            Spacer()
                                        }
                                    }
                                    
                                    VStack(alignment: .center){
                                        
                                        HStack(alignment: .center){
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["L6"]?.name, distance: vm.rideGridRidersRadarMap["L6"]?.distance)
                                            Spacer()
                                            
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["T1"]?.name, distance: vm.rideGridRidersRadarMap["T1"]?.distance)
                                            
                                            Spacer()
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["R6"]?.name, distance: vm.rideGridRidersRadarMap["R6"]?.distance)
                                        }.padding(.horizontal,16)
                                        
                                        
                                        HStack(alignment: .center){
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["L4"]?.name, distance: vm.rideGridRidersRadarMap["L4"]?.distance)
                                            Spacer()
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["R4"]?.name, distance: vm.rideGridRidersRadarMap["R4"]?.distance)
                                        }.padding(.horizontal,48)
                                        
                                        
                                        HStack(alignment: .center){
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["L2"]?.name, distance: vm.rideGridRidersRadarMap["L2"]?.distance)
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["L1"]?.name, distance: vm.rideGridRidersRadarMap["L1"]?.distance)
                                            Spacer()
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["R1"]?.name, distance: vm.rideGridRidersRadarMap["R1"]?.distance)
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["R2"]?.name, distance: vm.rideGridRidersRadarMap["R2"]?.distance)
                                        }.padding(.horizontal,8)
                                        
                                        
                                        
                                        HStack(alignment: .center){
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["L3"]?.name, distance: vm.rideGridRidersRadarMap["L3"]?.distance)
                                            Spacer()
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["R3"]?.name, distance: vm.rideGridRidersRadarMap["R3"]?.distance)
                                        }.padding(.horizontal,48)
                                        
                                        
                                        
                                        HStack(alignment: .center){
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["L5"]?.name, distance: vm.rideGridRidersRadarMap["L5"]?.distance)
                                            
                                            Spacer()
                                            
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["B1"]?.name, distance: vm.rideGridRidersRadarMap["B1"]?.distance)
                                            
                                            Spacer()
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["R5"]?.name, distance: vm.rideGridRidersRadarMap["R5"]?.distance)
                                        }.padding(.horizontal,16)
                                    }
                                    
                                    VStack(alignment: .center){
                                        
                                        HStack(alignment: .center){
                                            
                                            Spacer()
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["B14"]?.name, distance: vm.rideGridRidersRadarMap["B14"]?.distance)
                                            Spacer()
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["B2"]?.name, distance: vm.rideGridRidersRadarMap["B2"]?.distance)
                                            Spacer()
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["B13"]?.name, distance: vm.rideGridRidersRadarMap["B13"]?.distance)
                                            Spacer()
                                        }
                                        
                                        
                                        HStack(alignment: .center){
                                            Spacer()
                                            Spacer()
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["B3"]?.name, distance: vm.rideGridRidersRadarMap["B3"]?.distance)
                                            Spacer()
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["B4"]?.name, distance: vm.rideGridRidersRadarMap["B4"]?.distance)
                                            Spacer()
                                            Spacer()
                                        }
                                        
                                        
                                        HStack(alignment: .center){
                                            
                                            Spacer()
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["B7"]?.name, distance: vm.rideGridRidersRadarMap["B7"]?.distance)
                                            Spacer()
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["B5"]?.name, distance: vm.rideGridRidersRadarMap["B5"]?.distance)
                                            Spacer()
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["B6"]?.name, distance: vm.rideGridRidersRadarMap["B6"]?.distance)
                                            Spacer()
                                        }
                                        
                                        
                                        HStack(alignment: .center){
                                            
                                            Spacer()
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["B9"]?.name, distance: vm.rideGridRidersRadarMap["B9"]?.distance)
                                            Spacer()
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["B8"]?.name, distance: vm.rideGridRidersRadarMap["B8"]?.distance)
                                            Spacer()
                                        }
                                        
                                        
                                        HStack(alignment: .center){
                                            
                                            Spacer()
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["B12"]?.name, distance: vm.rideGridRidersRadarMap["B12"]?.distance)
                                            Spacer()
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["B10"]?.name, distance: vm.rideGridRidersRadarMap["B10"]?.distance)
                                            Spacer()
                                            RideGridUserMiniCapsuleView(name: vm.rideGridRidersRadarMap["B11"]?.name, distance: vm.rideGridRidersRadarMap["B11"]?.distance)
                                            Spacer()
                                        }
                                    }
                                    
                                }
                            }
                            
                        }
                        
                        ScrollView(.horizontal){
                            
                            HStack{
                                ForEach(vm.rideGridDevices, id: \.self){device in
                                    RideGridUserCapsuleView(name: device.name, distance: device.distance)
                                }
                            }.padding(.horizontal , 16)}
                        
                    }.frame( alignment: .center )
                }
            )
    }
}


struct RideGridUserMiniCapsuleView: View {
    
    func distanceLabel(distance:Int) -> String{
        switch distance{
        case 0:
            return "Nearby"
        case let n where n > 0:
            return  "\(distance)m"
        default:
            return "NO GPS"
        }
    }
    
    let name:String?
    let distance:Int?
    var body: some View {
        HStack(alignment: .center, spacing: 4){
            
            VStack(alignment: .leading, spacing: 0){
                if let name = name{
                    Text( "\(name)-\(distanceLabel(distance: distance ?? -1 ))")
                        .font(.system(.caption))
                        .fontWeight(.regular)
                        .foregroundColor(ColorTextPrimary)
                        .padding(.vertical, 2)
                        .padding(.horizontal, 6)
                        .background(Color(hex: 0xff1a1a1a))
                        .cornerRadius(100)
                    
                }else{
                    Text( "      ")
                        .font(.system(.caption))
                        .fontWeight(.regular)
                        .foregroundColor(ColorTextPrimary)
                        .padding(.vertical, 0)
                }
                
            }
        }
        
        
    }
    
}

struct RideGridUserCapsuleView: View {
   
    let name:String
    let distance:Int
    var body: some View {
        HStack(alignment: .center, spacing: 4){
            
            Image("helmet")
                .resizable()
                .scaledToFit()
                .frame( height: 22, alignment: .center)
            
            VStack(alignment: .leading, spacing: 0){
                
                Text(name)
                    .font(.system(.caption))
                    .fontWeight(.semibold)
                    .foregroundColor(ColorTextPrimary)
                    .padding(.vertical, 0)
                
                Text( distanceLabel(distance: distance ) )
                    .font(.system(.caption))
                    .fontWeight(.bold)
                    .foregroundColor(ColorTextPrimary)
                    .padding(.vertical, 0)
                
            }
            
        }
        .padding(.vertical, 3)
        .padding(.leading, 6)
        .padding(.trailing, 12)
        .background(Color(hex: 0xff1a1a1a))
        .cornerRadius(100)
        
    }
    
}

struct EdgeCardView<Content:View>: View {
    var bottomStroke:CGFloat = 3
    var backgroundColor:Color = ColorShark
    var bottomStrokeColor:Color = ColorSharkAccent
    var cornerRadius:CGFloat = 10
    let content: ()->Content
    
    var body: some View {
        ZStack(alignment: .center){
            ZStack(alignment: .center, content: content)
                .background(backgroundColor)
                .cornerRadius(cornerRadius)
                .padding(.bottom, bottomStroke)
        }
        .background(bottomStrokeColor)
        .cornerRadius(cornerRadius)
    }
}

//struct RideGridDashboardView_Previews: PreviewProvider {
//    static var previews: some View {
////        RideGridDashboardView()
//    }
//}


private func distanceLabel(distance:Int) -> String{
    switch distance{
    case 0:
        return "Nearby"
    case let n where n > 0:
        return  "\(distance)m"
    default:
        return "NO GPS"
    }
}
