//
//  RidegridCreateGroupView.swift
//  BluArmor-App
//
//  Created by Rahul Gaur on 26/01/22.
//

import SwiftUI

struct RidegridCreateGroupView: View {
    @ObservedObject var vm:BluArmorAppViewModel
    var body: some View {
        if #available(iOS 14.0, *) {
            RidegridCreateGroupContentView(vm:vm)
                        .navigationBarTitleDisplayMode(.inline)
                        .toolbar{
                            ToolbarItem(placement: .navigationBarLeading) {
                                Button(action:{
                                    self.vm.popBack()
                                }){
                                    Image("back.nav")
                                        .resizable()
                                        .scaledToFit()
                                        .frame( height: 44, alignment: .center)
                                }}
                        }
        }
        else{
            NavigationView{
                RidegridCreateGroupContentView(vm:vm)
                    .navigationBarTitle("",displayMode: .inline)
                    .navigationBarItems(leading: Button(action:{
                        self.vm.popBack()
                    }){
                        Image("back.nav")
                            .renderingMode(.original)
                            .resizable()
                            .scaledToFit()
                            .frame( height: 44, alignment: .center)
                    })
            }
        }
    }
}


struct RidegridCreateGroupContentView: View {
    @ObservedObject var vm:BluArmorAppViewModel
    
    var body: some View {
        BaseGradientView()
            .edgesIgnoringSafeArea(.all)
            .overlay(
                VStack(alignment: .center, spacing: 12){
                    
                    if let rideGridGroup = vm.rideGridGroupScanResult
                    {
                        Text("Found new RIDEGRID group")
                            .font(.system(.caption))
                            .fontWeight(.semibold)
                            .foregroundColor(ColorTextPrimaryAccent)
                        
                        TextFieldViewDisabled(text: rideGridGroup.ridegridGroup.name, label: "Group Name")
                            .padding(.horizontal, 16)
                            .padding(.vertical, 16)
                        
                        Button(action:{
                            vm.newRideGridGroupPrimary.toggle()
                        }){
                            HStack(alignment: .center, spacing: 10){
                                Image( vm.newRideGridGroupPrimary ? "radio.checked" : "radio.unchecked" )
                                    .resizable()
                                    .scaledToFit()
                                    .frame( height: 24, alignment: .center)
                                
                                Text("Set as primary ride group")
                                    .font(.system(.callout))
                                    .fontWeight(.semibold)
                                    .foregroundColor(ColorTextPrimaryAccent)
                                    .frame(maxWidth: .infinity,alignment: .leading)
                            }
                        }.padding(.horizontal, 16)
                        
                        HStack(alignment: .center, spacing: 12){
                            
                            Button(action:{ vm.clearRideGridGroupScanResult() }){
                                Text("Rescan".uppercased())
                                    .font(.system(.footnote))
                                    .fontWeight(.semibold)
                                    .foregroundColor(.white)
                                    .padding(.vertical, 10)
                                    .padding(.horizontal, 20)
                                    .overlay(
                                        RoundedRectangle(cornerRadius: 100)
                                            .stroke(.white, lineWidth: 2)
                                    )
                            }
                            
                            Button(action:{ vm.createRideGridGroupFromScan() }){
                                Text("Join".uppercased())
                                    .font(.system(.footnote))
                                    .fontWeight(.semibold)
                                    .foregroundColor(ColorShark)
                                    .padding(.vertical, 10)
                                    .padding(.horizontal, 20)
                                    .frame(maxWidth: .infinity)
                                    .background(Color.white)
                                    .cornerRadius(100)
                            }
                            
                        }
                        .padding(.horizontal, 16)
                        
                    }
                    else{
                        QrCodeScannerView()
                            .found(r: self.vm.onFoundQrCode)
                            .torchLight(isOn: self.vm.torchIsOn)
                            .interval(delay: self.vm.scanInterval)
                            .aspectRatio(1, contentMode: .fit)
                            .frame(maxWidth: .infinity)
                            .background(Color.white)
                            .cornerRadius(10)
                            .padding(.horizontal, 70)
                        
                        Text("Scan other rider’s QR code to join the group")   .font(.system(.caption))
                            .fontWeight(.semibold)
                            .foregroundColor(ColorTextPrimaryAccent)
                    }
                    
                   
                    
                    Spacer()
                    Spacer()
                        .frame(maxWidth: .infinity)
                        .frame( height: 2)
                        .background(Color.white)
                        .cornerRadius(10)
                        .padding(.horizontal, 120)
                    
                    Text("Create new Group")
                        .font(.system(.caption))
                        .fontWeight(.semibold)
                        .foregroundColor(ColorTextPrimaryAccent)
                    
                    Spacer()
                    
                    TextFieldView(text: $vm.newRideGridGroupName, label: "Group Name", suggestionText: "", showCharacterCount: true, maxCharacterCount: 10)
                        .padding(.horizontal, 16)
                    
                    Button(action:{
                        vm.newRideGridGroupPrimary.toggle()
                    }){
                        HStack(alignment: .center, spacing: 10){
                            Image( vm.newRideGridGroupPrimary ? "radio.checked" : "radio.unchecked" )
                                .resizable()
                                .scaledToFit()
                                .frame( height: 24, alignment: .center)
                            
                            Text("Set as primary ride group")
                                .font(.system(.callout))
                                .fontWeight(.semibold)
                                .foregroundColor(ColorTextPrimaryAccent)
                                .frame(maxWidth: .infinity,alignment: .leading)
                        }
                    }.padding(.horizontal, 16)
                    
                    Button(action:{
                        if vm.newRideGridGroupName.count > 0{
                            vm.createRideGridGroup()
                        }
                    }){
                        Text("Create".uppercased())
                            .font(.system(.footnote))
                            .fontWeight(.semibold)
                            .foregroundColor(ColorShark)
                            .padding(.horizontal, 18)
                            .padding(.vertical, 8)
                            .background(Color.white)
                            .cornerRadius(100)
                    }
                    .frame(maxWidth: .infinity,alignment: .trailing)
                    .padding(.horizontal, 16)
                    
                    // For the visibility of centent under FAB
                    
                }.frame( alignment: .center )
                
            )
    }
}


//struct RidegridCreateGroupView_Previews: PreviewProvider {
//    static var previews: some View {
//        RidegridCreateGroupView()
//    }
//}
