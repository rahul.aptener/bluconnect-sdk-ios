//
//  RideGridShareInviteView.swift
//  BluArmor-App
//
//  Created by Rahul Gaur on 31/01/22.
//

import SwiftUI
import CoreImage.CIFilterBuiltins

struct RideGridShareInviteView: View {
    @ObservedObject var vm:BluArmorAppViewModel
    
    var body: some View {
        if #available(iOS 14.0, *) {
            RideGridShareInviteContentView(vm:vm)
                .navigationBarTitleDisplayMode(.inline)
                .toolbar{
                    ToolbarItem(placement: .navigationBarLeading) {
                        Button(action:{
                            self.vm.popBack()
                        }){
                            Image("back.nav")
                                .resizable()
                                .scaledToFit()
                                .frame( height: 44, alignment: .center)
                        }}
                }
        }
        else{
            NavigationView{
                RideGridShareInviteContentView(vm:vm)
                    .navigationBarTitle("", displayMode: .inline)
                    .navigationBarItems(leading: Button(action:{
                        self.vm.popBack()
                    }){
                        Image("back.nav")
                            .renderingMode(.original)
                            .resizable()
                            .scaledToFit()
                            .frame( height: 44, alignment: .center)
                    })

            }
        }
    }
}


struct RideGridShareInviteContentView: View {
    @ObservedObject var vm:BluArmorAppViewModel
    
    var body: some View {
        BaseGradientView()
            .edgesIgnoringSafeArea(.all)
            .overlay(
                VStack(alignment: .center, spacing: 12){
                    
                    if let rideGridGroup = vm.inviteRideGridGroup,
                       let group = vm.getRidegridGroupInviteQrCodeData(rideGridGroup: rideGridGroup),
                       let qrCodeData = getQRCodeDate(text: group),
                       let uiImage = UIImage(data: qrCodeData)
                    {
                        TextFieldViewDisabled(text: rideGridGroup.name, label: "Group Name")
                            .padding(.horizontal, 16)
                            .padding(.vertical, 16)
                        
                        Image(uiImage: uiImage)
                            .resizable()
                            .frame(width: 200, height: 200)
                            .cornerRadius(8)
                    }
                    
                    Text("Scan the QR code to join the group")
                        .font(.system(.caption))
                        .fontWeight(.semibold)
                        .foregroundColor(ColorTextPrimaryAccent)
                    
                    Spacer()
                
                    // For the visibility of centent under FAB
                    
                }.frame( alignment: .center )
                
            )
    }
     
     func getQRCodeDate(text: String) -> Data? {
         guard let filter = CIFilter(name: "CIQRCodeGenerator") else { return nil }
         let data = text.data(using: .ascii, allowLossyConversion: false)
         filter.setValue(data, forKey: "inputMessage")
         guard let ciimage = filter.outputImage else { return nil }
         let transform = CGAffineTransform(scaleX: 10, y: 10)
         let scaledCIImage = ciimage.transformed(by: transform)
         let uiimage = UIImage(ciImage: scaledCIImage)
         return uiimage.pngData()!
     }
}

//struct RideGridShareInviteView_Previews: PreviewProvider {
//    static var previews: some View {
////        RideGridShareInviteView()
//    }
//}
