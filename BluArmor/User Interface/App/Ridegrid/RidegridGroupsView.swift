//
//  RideGridGroupView.swift
//  BluArmor-App
//
//  Created by Rahul Gaur on 31/01/22.
//

import SwiftUI
import BluConnect

struct RidegridGroupsView: View {
    @ObservedObject var vm:BluArmorAppViewModel
    
    var body: some View {
        if #available(iOS 14.0, *) {
            RidegridGroupsContentView(vm:vm)
                .navigationBarTitleDisplayMode(.inline)
                .toolbar{
                    ToolbarItem(placement: .navigationBarLeading) {
                        Button(action:{
                            self.vm.popBack()
                        }){
                            Image("back.nav")
                                .resizable()
                                .scaledToFit()
                                .frame( height: 44, alignment: .center)
                        }}
                    ToolbarItem(placement: .navigationBarTrailing) {
                        
                        Button(action:{
                            if  RideGridState.OFF == self.vm.rideGridState{
                                self.vm.rideGridAction(action: .TurnOn)
                            }
                            else {
                                self.vm.rideGridAction(action: .TurnOff)
                            }
                        }){
                            Text( self.vm.rideGridState == RideGridState.OFF ? "Turn On" : "Turn Off")
                                .font(.system(.caption))
                                .fontWeight(.semibold)
                                .foregroundColor(.white)
                                .padding(.horizontal, 12)
                                .padding(.vertical, 6)
                                .background( self.vm.rideGridState == RideGridState.OFF ? ColorPositive : ColorNegative)
                                .cornerRadius(100)
                        }}
                }
        }
        else{
            NavigationView{
                RidegridGroupsContentView(vm:vm)
                    .navigationBarTitle("",displayMode: .inline)
                    .navigationBarItems(leading:Button(action:{
                        self.vm.popBack()
                    }){
                        Image("back.nav")
                            .renderingMode(.original)
                            .resizable()
                            .scaledToFit()
                            .frame( height: 44, alignment: .center)
                    },trailing:  Button(action:{
                        if  RideGridState.OFF == self.vm.rideGridState{
                            self.vm.rideGridAction(action: .TurnOn)
                        }
                        else {
                            self.vm.rideGridAction(action: .TurnOff)
                        }
                    }){
                        Text( self.vm.rideGridState == RideGridState.OFF ? "Turn On" : "Turn Off")
                            .font(.system(.caption))
                            .fontWeight(.semibold)
                            .foregroundColor(.white)
                            .padding(.horizontal, 12)
                            .padding(.vertical, 6)
                            .background( self.vm.rideGridState == RideGridState.OFF ? ColorPositive : ColorNegative)
                            .cornerRadius(100)
                    })
            }
        }
    }
}


struct RidegridGroupsContentView: View {
    
    @ObservedObject var vm:BluArmorAppViewModel
    @State var newGroupErrorToast = false
    @State var groupModifyErrorToast = false
    var body: some View {
        BaseGradientView()
            .edgesIgnoringSafeArea(.all)
        
            .overlay(
                ScrollView(.vertical){
                    VStack(alignment: .center, spacing: 12){
                        
                        DeviceInfoCardCollapsed(deviceInfo: vm.deviceInfo, battery: vm.batteryLevel )
                        
                        HStack(){
                            Text("RIDEGRID Groups".uppercased())   .font(.system(.caption))
                                .fontWeight(.semibold)
                                .foregroundColor(ColorTextPrimary)
                                .padding(.leading, 12)
                                .frame(maxWidth:.infinity, alignment: .leading)
                            Spacer()
                            
                            Button(action:{vm.ridegridEditMode.toggle()}){
                                Text(( vm.ridegridEditMode ? "Done" : "Edit").uppercased())
                                    .font(.system(.caption))
                                    .fontWeight(.semibold)
                                    .padding(.horizontal, 10)
                                    .padding(.vertical, 4)
                                    .foregroundColor(ColorShark)
                                    .background(Color.white)
                                    .cornerRadius(100)
                            }
                            
                        }
                        .padding(.horizontal, 16)
                        
                        ScrollView(.horizontal){
                            HStack(alignment: .center, spacing: 12){
                                if vm.ridegridGroups.count < 5{
                                    Button(action:{
                                        if vm.rideGridState == .DISCOVERY || vm.rideGridState == .OFF{
                                            vm.navigateToRideGridCreateGroup()
                                        }else{
                                            newGroupErrorToast =  true
                                        }
                                        
                                    }){RideGridNewGroup()}
                                        .disabled(vm.ridegridEditMode)
                                }
                                
                                ForEach(vm.ridegridGroups , id: \.self){group in
                                    RideGridGroupView(
                                        editMode: $vm.ridegridEditMode,
                                        groupName: group.name,
                                        primaryGroup: group.active,
                                        onClick: {
                                            vm.navigateToRideGridDashboard(rideGridGroup: group)
                                        },
                                        removeGroup: {
                                            if vm.rideGridState == .DISCOVERY || vm.rideGridState == .OFF{
                                                vm.deleteRidegridGroup(rideGridGroup: group)
                                            }else{
                                                groupModifyErrorToast = true
                                            }
                                             },
                                        setPrimary: {
                                            if vm.rideGridState == .DISCOVERY || vm.rideGridState == .OFF{
                                                vm.markRideGridGroupPrimary(rideGridGroup: group)
                                            }else{
                                                groupModifyErrorToast = true
                                            }
                                            
                                        },
                                        priateGroup: group.privateGroup
                                    )
                                    
                                }
                                
//                                if let groups = vm.ridegridGroups{
//
//                                }
                            }
                            .padding(.horizontal, 16)
                        }
                        
                        HStack(){
                            Text("Active Devices".uppercased())   .font(.system(.caption))
                                .fontWeight(.semibold)
                                .foregroundColor(ColorTextPrimary)
                                .padding(.leading, 12)
                                .frame(maxWidth:.infinity, alignment: .leading)
                        }
                        .padding(.horizontal, 16)
                        
                        //                        List{
                        ForEach(Array(vm.rideGridDevices), id: \.self){ device in
                            RideGridUserView(name: device.name)
                        }
                        //                        }
                        
                    }.frame( alignment: .center )
                }
                    .toast(active: newGroupErrorToast, alertOver:{ newGroupErrorToast = false },content:
                            ToastMessage(message: "Can not create new RIDEGRID group when RIDEGRID is connected!")
                          )
                    .toast(active: groupModifyErrorToast, alertOver:{ groupModifyErrorToast = false },content:
                                    ToastMessage(message: "RIDEGRID group modification is not allowed in when RIDEGRID is connected!")
                                  )
            )
        
    }
}

struct ToastMessage:View{
    let message:String
    var body: some View {
        VStack(){
            Spacer()
            Text(message)
                .font(.system(size: 12))
                .padding(.vertical,8)
                .padding(.horizontal, 12)
                .foregroundColor(Color.black)
                .background(Color(0x80FFFFFF))
                .cornerRadius(10)
                .padding(10)
        }
    }
}

extension View{
    func toast<Content:View>(active:Bool, alertOver: @escaping ()->(), content:Content ) -> some View {
        return self.overlay(ToastView(alert: active, alertOver: alertOver, content: content))
    }
}


struct ToastView<Content:View>: View {
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    @State private var alertActivity = Date()
    let alert:Bool
    let alertOver:()->()
    let content:Content
    func timerTick(){
        if Date() >= alertActivity.addingTimeInterval(2*1) {
            alertOver()
        }
    }
    var body: some View {
        
        if(alert){
            content
                .onReceive(timer) { time in
                    timerTick()
                }
                .onAppear{
                    alertActivity = Date()
                }
        }else{
            Spacer()
        }
        
    }
}
struct RideGridUserView: View {
    
    let name:String
    var body: some View {
        HStack(alignment: .center, spacing: 4){
            Image("helmet")
                .resizable()
                .scaledToFit()
                .frame( height: 22, alignment: .center)
            
            VStack(alignment: .leading, spacing: 0){
                Text(name)
                    .font(.system(.caption))
                    .fontWeight(.semibold)
                    .foregroundColor(ColorTextPrimary)
                    .padding(.vertical, 0)
                Text("RIDEGRID")
                    .font(.system(.caption))
                    .fontWeight(.bold)
                    .foregroundColor(ColorTextPrimary)
                    .padding(.vertical, 0)
            }
        }
        .padding(.vertical, 3)
        .padding(.leading, 6)
        .padding(.trailing, 12)
    }
    
}


struct RideGridNewGroup: View {
    
    var body: some View {
        VStack(alignment: .center, spacing: 0){
            Image("bubble.square")
                .renderingMode(.original)
                .resizable()
                .scaledToFit()
                .foregroundColor(Color(hex: 0x77FFFFFF))
                .frame( height: 18, alignment: .center)
                .padding(.bottom, 12)
            
            Text("New".uppercased())
                .font(.system(.callout))
                .fontWeight(.bold)
                .foregroundColor(ColorTextPrimary)
            
            Text("Group".uppercased())
                .font(.system(.footnote))
                .fontWeight(.semibold)
                .foregroundColor(ColorTextPrimaryAccent)
            
        }
        .padding(.horizontal, 18)
        .frame(minWidth: 70, minHeight: 98)
        .background(Color(hex: 0xff2E2E36))
        .cornerRadius(10)
    }
}

struct RideGridGroupView: View {
    @Binding var editMode:Bool
    
    let groupName:String
    let primaryGroup:Bool
    var onClick:()->Void
    var removeGroup:()->Void
    var setPrimary:()->Void
    var priateGroup:Bool
    
    var body: some View {
        ZStack{
            Button(action:self.onClick){
                VStack(alignment: .center, spacing: 2){
                    Text((groupName.first?.uppercased()) ?? "")
                        .font(.system(.largeTitle))
                        .fontWeight(.heavy)
                        .foregroundColor(Color(hex: 0x77FFFFFF))
                    
                    Text(groupName.uppercased())
                        .font(.system(.footnote))
                        .fontWeight(.semibold)
                        .foregroundColor(ColorTextPrimary)
                        .padding(.horizontal, 4)
                }
                .frame(minWidth: 104,minHeight: 98)
                .background(Color(hex: 0xff4567C4))
                .cornerRadius(10)
                .padding(.vertical, primaryGroup || editMode ? 9 : 0)
                .padding(.horizontal, editMode ? 6 : 0)
            }
            .disabled(editMode)
            VStack{
                
                HStack{
                    if primaryGroup {
                        Spacer()
                        
                        Text("Primary".uppercased())
                            .font(.system(.caption))
                            .fontWeight(.semibold)
                            .padding(.horizontal, 6)
                            .padding(.vertical, 3)
                            .foregroundColor(ColorShark)
                            .background(Color.white)
                            .cornerRadius(100)
                            .frame(alignment: .top)
                    }
                    Spacer()
                    
                    if (editMode && priateGroup){
                        Button(action: self.removeGroup){Image("remove.round")
                                .resizable()
                                .scaledToFit()
                                .frame( height: 28, alignment: .center)
                                .frame(alignment: .topTrailing)
                        } }
                }
                Spacer()
                if editMode && !primaryGroup{
                    Button(action: self.setPrimary){
                        Text("Set Primary".uppercased())
                            .font(.system(.caption))
                            .fontWeight(.semibold)
                            .padding(.horizontal, 6)
                            .padding(.vertical, 3)
                            .foregroundColor(ColorShark)
                            .background(Color.white)
                            .cornerRadius(100)
                            .frame(alignment: .bottom)
                    }}
            }
        }
    }
}

//struct RideGridGroupView_Previews: PreviewProvider {
//    static var previews: some View {
//        RidegridGroupsView()
//    }
//}
