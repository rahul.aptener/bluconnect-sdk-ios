//
//  SpeedDialContactsView.swift
//  BluArmor-App
//
//  Created by Rahul Gaur on 26/01/22.
//

import SwiftUI
import BluConnect

struct SpeedDialContactsView: View {
    @Binding var contacts:[Buddy?]
    let action:(Buddy) -> Void
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false){
            HStack( spacing: 12){
                ForEach(0..<contacts.count) { i in
                    if let contact = contacts[i]{
                        Button(action:{self.action(contact)}){ SpeedDialContactView(position: i+1, contactName: contact.name) }
                    }else{
                        SpeedDialFreeSlotView(position: i+1)
                    }
                }
            }
            .padding(.horizontal,16)
        }
    }
}

struct SpeedDialContactView: View {
    let position:Int
    let contactName:String
    var body: some View {
        VStack(alignment: .center, spacing: 4){
            Text("\(position)")
                .font(.system(.title))
                .fontWeight(.bold)
                .foregroundColor(Color(hex: 0x77FFFFFF))
            
            Image("helmet")
                .resizable()
                .scaledToFit()
                .frame( width: 28, alignment: .center)
            
            Text(contactName)
                .font(.system(.caption))
                .fontWeight(.bold)
                .foregroundColor(ColorTextPrimary)
        }
        .padding(.horizontal, 18)
        .frame(minWidth: 102, minHeight: 98)
        .background(ColorShark)
        .cornerRadius(10)
    }
}

struct SpeedDialFreeSlotView: View {
    let position:Int
    var body: some View {
        VStack(alignment: .center, spacing: 0){
            Text("Slot")
                .font(.system(.body))
                .foregroundColor(ColorTextPrimary)
            Text("\(position)")
                .font(.system(.largeTitle))
                .fontWeight(.bold)
                .foregroundColor(Color(hex: 0x77FFFFFF))
            Text("Free")
                .font(.system(.caption))
                .fontWeight(.bold)
                .foregroundColor(ColorTextPrimary)
        }
        .padding(.horizontal, 18)
        .frame(minWidth: 70, minHeight: 98)
        .background(ColorShark)
        .cornerRadius(10)
    }
}
