//
//  LandingViewModel.swift
//  BluArmor-App
//
//  Created by Rahul Gaur on 24/01/22.
//

import Foundation
import Combine

protocol LandingViewModel:ObservableObject{
}

class LandingViewModelImpl: LandingViewModel{
    @Published var can:Bool = false
    
    @Published var appDestination: String? = AppDestination.LandingDest.identifier
    
    var currentTimePublisher = Timer.TimerPublisher(interval: 1.0, runLoop: .main, mode: .default)
    var cancellable: AnyCancellable?
    
    init(){
        startTimer()
    }
    
    func startTimer(){
        DispatchQueue.main.async{
            sleep(1)
            self.appDestination = AppDestination.DiscoveryDest.identifier
        }
    }
    
    deinit {
        self.cancellable?.cancel()
    }
}
