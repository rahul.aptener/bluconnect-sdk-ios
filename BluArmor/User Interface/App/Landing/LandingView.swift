//
//  LandingView.swift
//  BluArmor-App
//
//  Created by Rahul Gaur on 23/01/22.
//

import SwiftUI

struct LandingView: View {
    var body: some View {
        NavigationView{
         SplashGradientView()
         .edgesIgnoringSafeArea(.all)
         .navigationBarTitle("", displayMode: .inline)
         //            .navigationBarTitleDisplayMode(.inline)
         .overlay(
         VStack{
         Image("icon")
         .resizable()
         .scaledToFit()
         .frame( height: 82, alignment: .center)
         
         Spacer(minLength: 0)
         .frame( height: 16)
         
         Image("TextIcon")
         .resizable()
         .scaledToFit()
         .frame( height: 48, alignment: .center)
         }
         )}
    }
}
//
//struct LandingView_Previews: PreviewProvider {
//    private func navigate(appDestination: AppDestination){
//    }
//
//    static var previews: some View {
//        LandingView(vm: LandingViewModelImpl(navigate: self.navigate(appDestination:)))
//            .environment(\.managedObjectContext, PersistenceController.preview.container.viewContext).previewInterfaceOrientation(.portrait)
//    }
//}
