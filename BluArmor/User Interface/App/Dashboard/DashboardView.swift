//
//  LandingView.swift
//  BluArmor
//
//  Created by Rahul Gaur on 04/01/22.
//

import SwiftUI
import BluConnect

struct DashboardContentView: View {
    @ObservedObject var vm:BluArmorAppViewModel

    init(vm:BluArmorAppViewModel){
        self.vm = vm
    }
    
    @State var dyanamicVolume = false
    var body: some View {
        BaseGradientView()
            .edgesIgnoringSafeArea(.all)
            .overlay(
                ZStack{
                    ScrollView(.vertical){
                        VStack(alignment: .center, spacing: 12){
                            
                            DeviceInfoCard(
                                deviceInfo:vm.deviceInfo,
                                device: vm.device ,
                                           battery: vm.batteryLevel,
                                           mediaPlayerState: vm.mediaPlayerState,
                                           musicAction: vm.musicAction(action:),
                                           dialerState: vm.dialerState,
                                           dialerAction:vm.dialerAction(action:)
                            )
                            
                            if vm.updateAvailable{
                                
                                SwipeView(content:
                                            VStack(alignment: .leading ){
                                                Text("Firmware Update Available!".uppercased())
                                                    .font(.system(.subheadline))
                                                    .fontWeight(.semibold)
                                                    .foregroundColor(ColorTextPrimary)
                                                    .frame(maxWidth:.infinity)
                                                Text("Swipe right to update".uppercased()).font(.system(.caption))
                                                    .fontWeight(.regular)
                                                    .foregroundColor(ColorTextPrimaryAccent)
                                                    .frame(maxWidth:.infinity)
                                                
                                            }.frame(
                                                maxWidth:.infinity,
                                                minHeight: 74
                                            )
                                            .background(Color(0xFFFF455B))
                                            .cornerRadius(8),
                                          action: { vm.rebootInOtaMode() }
                                )
                                .frame(maxWidth: .infinity, minHeight: 48,alignment: .leading)
                                .padding(.horizontal,16)
                                .padding(.vertical, 14)
                            }
                            
                            HStack(alignment: .center, spacing: 12){
                                if vm.deviceModel == BluArmorModel.SX_20{
                                    Button(action:{
                                        self.vm.navigateTo(appDestination: .RideGridGroupsDest)
                                    }){
                                        VStack{
                                            Image("ridegrid")
                                                .renderingMode(.template)
                                                .resizable()
                                                .scaledToFit()
                                                .frame(height: 22, alignment: .center)
                                                .foregroundColor(.white)
                                            
                                            Text("RIDEGRID".uppercased())
                                                .font(.system(.subheadline))
                                                .fontWeight(.semibold)
                                                .foregroundColor(ColorTextPrimary)
                                            
                                            Text("Intercom".uppercased())   .font(.system(.caption))
                                                .fontWeight(.regular)
                                                .foregroundColor(ColorTextPrimaryAccent)
                                            
                                        }.frame(
                                            maxWidth:.infinity,
                                            minHeight: 74
                                        )
                                        .background(ColorSharkDark)
                                        .cornerRadius(8)}
                                }
                                
                                Button(action:{
                                    self.vm.navigateTo(appDestination: .RideLynkDiscoveryDest)
                                }){
                                    
                                    VStack{
                                        Image("ridelynk")
                                            .renderingMode(.template)
                                            .resizable()
                                            .scaledToFit()
                                            .frame(height: 22, alignment: .center)
                                            .foregroundColor(.white)
                                        
                                        Text("RIDELYNK".uppercased())
                                            .font(.system(.subheadline))
                                            .fontWeight(.semibold)
                                            .foregroundColor(ColorTextPrimary)
                                        
                                        Text("Intercom".uppercased())   .font(.system(.caption))
                                            .fontWeight(.regular)
                                            .foregroundColor(ColorTextPrimaryAccent)
                                        
                                    }
                                    .frame( maxWidth:.infinity,  minHeight: 74 )
                                    .background(ColorSharkDark)
                                    .cornerRadius(8)}
                                
                                Button(action:{
                                    vm.navigateTo(appDestination: .BuddyListDest)
                                }){
                                    
                                    VStack{
                                        Image("riders")
                                            .renderingMode(.template)
                                            .resizable()
                                            .scaledToFit()
                                            .frame(height: 22, alignment: .center)
                                            .foregroundColor(.white)
                                        
                                        Text("Buddy".uppercased())
                                            .font(.system(.subheadline))
                                            .fontWeight(.semibold)
                                            .foregroundColor(ColorTextPrimary)
                                        
                                        Text("List".uppercased())   .font(.system(.caption))
                                            .fontWeight(.regular)
                                            .foregroundColor(ColorTextPrimaryAccent)
                                        
                                    }.frame(
                                        maxWidth:.infinity,
                                        minHeight: 74
                                    )
                                    .background(ColorSharkDark)
                                    .cornerRadius(8)
                                }
                            }
                            .frame(maxWidth: .infinity)
                            .padding(.horizontal, 16)
                            
                            VStack(alignment: .leading, spacing: 4){
                                HStack(){
                                    Text("Volume".uppercased())   .font(.system(.caption))
                                        .fontWeight(.semibold)
                                        .foregroundColor(ColorTextPrimaryAccent)
                                        .padding(.leading, 12)
                                }
                                
                                HStack{
                                    BigSliderView(
                                        maxLevel: 15,
                                        level: Float( vm.volumeLevel?.volumeLevel ?? 0),
                                        onProgressCallBack: {level in
                                            vm.volumeControlAction(action: .ChangeVolume(level: level))
                                        })
                                    .frame(maxWidth: .infinity, minHeight: 48,alignment: .leading)
                                    
                                    Button(action: {
                                        self.dyanamicVolume = !dyanamicVolume
                                        //                                        vm.volumeControlAction(action: .ToggleDynamicVolume(toggleOn: !(vm.volumeLevel?.dynamicMode ?? false )))
                                    }){
                                        VStack(alignment: .center, spacing: 4){
                                            Text("Dynamic")
                                                .font(.system(.caption))
                                                .fontWeight(.semibold)
                                                .foregroundColor(ColorTextPrimaryAccent)
                                            
                                            Spacer(minLength: 2)
                                                .frame(width: 26, height:3, alignment: .center)
                                                .background(self.dyanamicVolume ? ColorPositive : ColorNegative)
                                                .cornerRadius(10)
                                            
                                        }.padding(6)
                                            .frame(minHeight: 48,alignment:.center)
                                            .background(ColorSharkAccent)
                                            .cornerRadius(8)
                                    }
                                }
                                .padding(8)
                                .background(ColorSharkDark)
                                .cornerRadius(8)
                            }
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .padding(.horizontal, 16)
                            
                            HStack(){
                                Text("Speed Dial".uppercased())   .font(.system(.caption))
                                    .fontWeight(.semibold)
                                    .foregroundColor(ColorTextPrimaryAccent)
                                    .padding(.leading, 12)
                            }.frame(maxWidth: .infinity, alignment: .leading)
                                .padding(.horizontal, 16)
                            
                            SpeedDialContactsView(contacts: $vm.speedDialDevices, action: {
                                vm.connectSpeedDialDevice(speedDialDevice: $0)
                            })
                            // For the visibility of centent under FAB
                            Spacer(minLength: 0).frame( width:0, height: 52)
                        }.frame( alignment: .center )
                    }
                    Button(action: {
                        vm.device?.voiceAssistant.invokeAssistant()
                    }, label: {
                        Text("Siri".uppercased())
                            .font(.system(.footnote))
                            .fontWeight(.semibold)
                            .foregroundColor(ColorShark)
                            .padding(.vertical, 12)
                            .padding(.horizontal , 34)
                    })
                    .background(Color.white)
                    .cornerRadius(38.5)
                    .padding()
                    .frame(
                        maxWidth: .infinity,
                        maxHeight: .infinity,
                        alignment: .bottomTrailing
                    )
                    .shadow(color: Color.black.opacity(0.3),
                            radius: 3,
                            x: 3,
                            y: 3)
                    //                        .frame( minWidth: .infinity, alignment: .bottomTrailing)
                    
                }
                
            )
    }
}

struct DashboardView: View {
    @ObservedObject var vm:BluArmorAppViewModel
    var body: some View {
        if #available(iOS 14.0, *) {
            DashboardContentView(vm:vm)
                .navigationBarTitleDisplayMode(.inline)
                .toolbar{
                    ToolbarItem(placement: .principal) {
                        Image("TextIcon")
                            .resizable()
                            .scaledToFit()
                            .frame( height: 28, alignment: .center)
                    }
                    ToolbarItem(placement: .navigationBarTrailing) {
                        Button(action:{
                            self.vm.navigateTo(appDestination: .PreferenceDest)
                        }){
                            Image("preference")
                                .resizable()
                                .scaledToFit()
                                .frame( height:44, alignment:.trailing)
                        }}
                }
        }
        else{
            NavigationView{
                DashboardContentView(vm:vm)
                    .navigationBarTitle("", displayMode: .inline)
                
                    .navigationBarItems(trailing: Button(action:{self.vm.navigateTo(appDestination: .PreferenceDest)}){
                        Image("preference")
                            .renderingMode(.original)
                            .resizable()
                            .scaledToFit()
                            .frame( height:44)
                        
                    })
            }
        }
    }
}


struct DeviceInfoCard: View {
    var deviceInfo:DeviceDetails?
    
    var device:ConnectedBluArmorDevice?
    var battery:BatteryLevel?
    
    var mediaPlayerState:MediaPlayerState?
    var musicAction:(MusicActions) -> Void
    
    var dialerState:DialerState?
    var dialerAction:(DialerActions) -> Void
    
    var body: some View {
        ZStack(alignment: .topLeading){
            LinearGradient(gradient: Gradient(colors: [Color(hex: 0xFF2E5A80),Color(hex: 0xFF254C6D),Color(hex: 0xFF0C2941)]), startPoint: .leading, endPoint: .trailing)
            .cornerRadius(8)
            .padding(.top ,18)
            
            Image("Sx20Small")
                .resizable()
                .aspectRatio( contentMode:.fit)
                .scaledToFit()
                .frame(width: 78, alignment: .leading)
            
            VStack(alignment: .leading, spacing: 10){
                HStack(alignment: .top, spacing: 4){
                    VStack(alignment: .leading){
                        Text(deviceInfo?.deviceName ?? "Disconnected")
                            .font(.system(.subheadline)).fontWeight(.semibold)
                            .foregroundColor(ColorTextPrimary)
                        if let modelLabel = device?.model.label{
                            Text(modelLabel)
                                .font(.system(.caption))
                                .fontWeight(.regular)
                                .foregroundColor(ColorTextPrimaryAccent)
                        }
                    }
                    .frame(
                        maxWidth: .infinity,
                        maxHeight: .infinity,
                        alignment: .leading
                    )
                    
                    if let batt = battery{
                        Text("\(batt.level)%").font(.system(.caption)).fontWeight(.semibold)
                            .foregroundColor(ColorTextPrimary)
                    }
                }
                .padding(.leading, 78)
                .padding(.trailing, 16)
                
                if let dialerState = dialerState{
                    if (dialerState == .Idle || dialerState == .Missed || dialerState == .Unknown){
                        MusicControlsCard(mediaPlayerState: self.mediaPlayerState, musicAction: self.musicAction)
                    }
                    else {
                        DialerControlsCard(dialerState: self.dialerState, dialerAction: self.dialerAction)
                    }
                }else {
                    MusicControlsCard(mediaPlayerState: self.mediaPlayerState, musicAction: self.musicAction)
                }
                
            }
            .padding(.top ,18)
            .padding(.vertical, 10)
            
        }
        .padding(.horizontal, 16)
    }
}

struct MusicControlsCard: View {
    var mediaPlayerState:MediaPlayerState?
    var musicAction:(MusicActions) -> Void
    
    var body: some View {
        HStack(alignment: .center, spacing: 0){
            VStack(alignment: .leading){
                if let mediaPlayerState = mediaPlayerState, case MediaPlayerState.Playing(let track, let artist) = mediaPlayerState {
                    Text(track ?? " ")
                        .font(.system(.subheadline)).fontWeight(.semibold)
                        .foregroundColor(ColorTextPrimary)
                        .lineLimit(1)
                    
                    Text(artist ?? " ")
                        .fontWeight(.regular)
                        .foregroundColor(ColorTextPrimaryAccent)
                        .lineLimit(1)
                    
                }
                else { Text(" ")
                        .font(.system(.subheadline)).fontWeight(.semibold)
                        .foregroundColor(ColorTextPrimary)
                        .lineLimit(1)
                    
                    Text(" ")
                        .fontWeight(.regular)
                        .foregroundColor(ColorTextPrimaryAccent)
                        .lineLimit(1)
                    
                }
            }
            .padding(.horizontal, 16)
            .frame(
                maxWidth: .infinity,
                alignment: .leading
            )
            
            
            
            Button(action: {
                self.musicAction(.PREVIOUS)
            }){
                Image(systemName: "backward.end.fill")
                    .resizable()
                    .aspectRatio( contentMode:.fit)
                    .scaledToFit()
                    .frame(width: 18,height: 14, alignment: .center)
                    .padding(.horizontal, 16)
                    .padding(.vertical, 11)
                    .foregroundColor(Color.white)
            }
            
            
            if let mediaPlayerState = mediaPlayerState, case MediaPlayerState.Playing(_, _) = mediaPlayerState {
                Button(action: {
                    self.musicAction(.PAUSE)
                }){
                    Image(systemName: "pause.fill")
                        .resizable()
                        .aspectRatio( contentMode:.fit)
                        .scaledToFit()
                        .frame(width: 22,height: 20, alignment: .center)
                        .padding(.horizontal, 16)
                        .padding(.vertical, 8)
                        .foregroundColor(Color.white)
                }
            }
            else {
                Button(action: {
                    self.musicAction(.PLAY)
                }){
                    Image(systemName: "play.fill")
                        .resizable()
                        .aspectRatio( contentMode:.fit)
                        .scaledToFit()
                        .frame(width: 22,height: 20, alignment: .center)
                        .padding(.horizontal, 16)
                        .padding(.vertical, 8)
                        .foregroundColor(Color.white)
                }
            }
            
            Button(action: {
                self.musicAction(.NEXT)
            }){
                Image(systemName: "forward.end.fill")
                    .resizable()
                    .aspectRatio( contentMode:.fit)
                    .scaledToFit()
                    .frame(width: 18,height: 14, alignment: .center)
                    .padding(.horizontal, 16)
                    .padding(.vertical, 11)
                    .foregroundColor(Color.white)
            }
            
            
            
            
            
            
        }
    }
}

struct DialerControlsCard: View {
    
    var dialerState:DialerState?
    var dialerAction:(DialerActions) -> Void
    
    var body: some View {
        HStack(alignment: .center, spacing: 0){
            VStack(alignment: .leading){
                if let dialerState = dialerState, case DialerState.Incoming(let phoneNumber, let name) = dialerState{
                    Text(name ?? " ")
                        .font(.system(.subheadline)).fontWeight(.semibold)
                        .foregroundColor(ColorTextPrimary)
                        .lineLimit(1)
                    Text(phoneNumber ?? " ")
                        .font(.system(.caption))
                        .fontWeight(.regular)
                        .foregroundColor(ColorTextPrimaryAccent)
                        .lineLimit(1)
                }
                else if let dialerState = dialerState, case DialerState.Active (let phoneNumber, let name) = dialerState{
                    Text(name ?? " ")
                        .font(.system(.subheadline)).fontWeight(.semibold)
                        .foregroundColor(ColorTextPrimary)
                        .lineLimit(1)
                    Text(phoneNumber ?? " ")
                        .font(.system(.caption))
                        .fontWeight(.regular)
                        .foregroundColor(ColorTextPrimaryAccent)
                        .lineLimit(1)
                }
                else {
                    Text(" ")
                        .font(.system(.subheadline)).fontWeight(.semibold)
                        .foregroundColor(ColorTextPrimary)
                        .lineLimit(1)
                    Text(" ")
                        .font(.system(.caption))
                        .fontWeight(.regular)
                        .foregroundColor(ColorTextPrimaryAccent)
                        .lineLimit(1)
                }
                
                
                
            }
            .padding(.horizontal, 16)
            .frame(
                maxWidth: .infinity,
                alignment: .leading
            )
            if let dialerState = dialerState, case DialerState.Incoming(_, _) = dialerState{
                Button(action:{
                    self.dialerAction(.ANSWER)
                })
                {
                    Image(systemName: "phone.circle.fill")
                        .resizable()
                        .aspectRatio( contentMode:.fit)
                        .scaledToFit()
                        .frame(width: 38,height: 38, alignment: .center)
                        .padding(6)
                        .padding(.horizontal, 6)
                        .foregroundColor(ColorPositive)
                }
            }
            
            Button(action:{
                if let dialerState = dialerState, case DialerState.Incoming(_, _) = dialerState{
                    self.dialerAction(.REJECT)
                }else {
                    self.dialerAction(.END)
                }
            })
            {
                Image(systemName: "phone.down.circle.fill")
                    .resizable()
                    .aspectRatio( contentMode:.fit)
                    .scaledToFit()
                    .frame(width: 38,height: 38, alignment: .center)
                    .padding(6)
                    .padding(.horizontal, 6)
                    .foregroundColor(ColorNegative)
            }
        }
    }
}
//
//struct DashboardView_Previews: PreviewProvider {
//    static var previews: some View {
//        DashboardView().environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
////            .previewInterfaceOrientation(.portrait)
//    }
//}
