//
//  OtaDashboardView.swift
//  BluArmor
//
//  Created by Rahul Gaur on 02/09/22.
//

import Foundation
import SwiftUI

struct OtaContentView: View {
    @ObservedObject var vm:BluArmorAppViewModel
    
    var body: some View {
        BaseGradientView()
            .edgesIgnoringSafeArea(.all)
            .overlay(
                VStack(alignment: .center, spacing: 12){
                    VStack(alignment: .leading){
                        
                        Spacer()
                        
                        Image("sx20")
                            .resizable()
                            .frame(width: 320, height: 320)
                        
//
//                        Image("sx20")
//                            .renderingMode(.original)
//                            .resizable()
//                            .scaledToFit()
//                            .frame(width: 320, alignment: .center)
//                            .frame(
//                                maxWidth: .infinity,
//                                maxHeight: .infinity,
//                                alignment: .center
//                            )
//                            .aspectRatio( contentMode:.fit)

                        
                        Spacer()
                        
                        HStack(alignment: .center){
                           Spacer()
                            OtaProgressStep(progress: CGFloat(Double(vm.firmwareUpdateProgress) * 0.01))
                            Spacer()
                        }
                        
                        
                        Spacer()
                    }
                    .frame(
                        maxWidth: .infinity,
                        maxHeight: .infinity,
                        alignment: .leading
                    )
                    .padding(.horizontal, 32)
                    
                    Spacer(minLength: 0).frame( width:0, height: 52)
                    
                    
                }.frame( alignment: .center )
            )
    }
}

private struct OtaProgressStep: View {
    let progress:CGFloat
    
    
    var body: some View {
        VStack( spacing: 0){
            ZStack(alignment: .center){
                Text("\(Int(progress * 100.0))")
            }
            .frame(width: 80, height: 80)
            .aspectRatio(1, contentMode: .fill)
//            .background(Circle().fill(Color.white))
            .padding(2)
            .background(Circle().trim(from: 0, to: progress)
                .stroke(Color(0xFF44C26B),style: StrokeStyle(lineWidth: 4, lineCap: .round))
                .rotationEffect(.degrees(90)))
            
        }.frame(width: 120)
    }
}


struct OtaView: View {
    @ObservedObject var vm:BluArmorAppViewModel
    
    var body: some View {
        if #available(iOS 14.0, *) {
            OtaContentView(vm:vm)
                .navigationBarTitle("", displayMode: .inline)
//                .navigationBarItems(trailing: Button(action:{
//                    self.vm.navigateTo(appDestination: .PreferenceDest)
//                }){
//                    Image("preference")
//                        .renderingMode(.original)
//                        .resizable()
//                        .scaledToFit()
//                        .frame( height:44, alignment:.trailing)
//                })
        }
        else{
            NavigationView{
                OtaContentView(vm:vm)
                    .navigationBarTitle("", displayMode: .inline)
//                    .navigationBarItems(trailing: Button(action:{
//                        self.vm.navigateTo(appDestination: .PreferenceDest)
//                    }){
//                        Image("preference")
//                            .renderingMode(.original)
//                            .resizable()
//                            .scaledToFit()
//                            .frame( height:44, alignment:.trailing)
//                    })
            }
        }
    }
}
