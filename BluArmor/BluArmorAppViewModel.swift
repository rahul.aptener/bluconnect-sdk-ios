//
//  BluArmorViewModel.swift
//  BluArmor
//
//  Created by Rahul Gaur on 17/03/22.
//

import Foundation
import Combine
import BluConnect
import AVFoundation

class BluConnectManager:BluConnectService{
    var _onDeviceReady: (_ device:ConnectedBluArmorDevice) -> Void
    var _onConnectionStateChange: (_ connectionState:ConnectionState) -> Void
    
    init(onDeviceReady: @escaping (_ device:ConnectedBluArmorDevice) -> Void, onConnectionStateChange: @escaping (_ connectionState:ConnectionState) -> Void){
        self._onConnectionStateChange = onConnectionStateChange
        self._onDeviceReady = onDeviceReady
        super.init()
        
        //        self.startTimer()
        //        self.handleConnectionState()
        //        self.refreshRideGridGroups()
        //        self.refreshSpeedDialList()
    }
    override func onConnectionStateChange(state: ConnectionState) {
//        "\(#function): \(state)".logE
        
        self._onConnectionStateChange(state)
        
        switch state {
        case .ScannerNotReady(let reason):return
        case .Connecting(let bluArmorDevice):return
        case .Connected(let bluArmorDevice):return
        case .Failed(let bluArmorDevice):return
        case .DeviceReady(let bluArmorDevice): self._onDeviceReady(bluArmorDevice)
        case .Disconnecting(let bluArmorDevice):return
        case .Discovery(let bluArmorDevices):return
        }
    }
    
}

class BluArmorAppViewModel:SpeedDialEditViewModel,
                           BuddyListViewModel,
                           ObservableObject
{
    @Published var deviceName:String?
    @Published var deviceModel:BluArmorModel?

    lazy var bluConnectManager:BluConnectManager = BluConnectManager(onDeviceReady: onDeviceReady(device:), onConnectionStateChange: onConnectionStateChange(state:))
    
    @Published public var count: Int = 0
    func increment(){
        self.count += 1
    }
    
    var myRouter: MyRouter? = nil
    //    @Published var appDestination: String? = AppDestination.LandingDest.identifier
    //    private var appDestStack:[AppDestination] = []
    
    func navigateTo(appDestination: AppDestination, withBackStack:Bool = true){
        myRouter?.navigateTo(destination: appDestination)
        
        //        "\(#function) \(appDestination), \(appDestStack.last?.identifier), \(appDestination.identifier)".logD
        //        if appDestStack.last != appDestination{
        //            if withBackStack{
        //                self.appDestStack.append(appDestination)
        //            }
        //            self.appDestination = appDestination.identifier
        //        }
    }
    
    func popBack(){
        myRouter?.pop()
        
        //        "\(#function) \(appDestStack)".logD
        //        if let _ = appDestStack.popLast(), let dest = appDestStack.popLast()  {
        //            navigateTo(appDestination: dest)
        //        }
    }
    
    func popBackTo(popDestination:AppDestination, includingDest:Bool = false){
        myRouter?.navigateTo(destination: popDestination)
        
        //        "\(#function) \(popDestination)".logD
        //        if let position = appDestStack.firstIndex(of: popDestination){
        //            appDestStack.removeLast(appDestStack.count - position)
        //            if includingDest{
        //                popBack()
        //            }else {
        //                navigateTo(appDestination: popDestination)
        //            }
        //        }
    }
    
    @Published var device:S20X? = nil
    @Published var otaDevice:S20XOta? = nil

    @Published var firmwareUpdateProgress: Int = 0
    @Published var ridegridUpdateProgress: Int = 0
    @Published var firmwareUpdateState: FirmwareUpdateState? = nil
    
    @Published var updateAvailable: Bool = false
    @Published var otaSwitchState: OtaSwitchState? = nil

    @Published var connectionState: ConnectionState? = nil
    @Published var deviceInfo: DeviceDetails? = nil
    @Published var batteryLevel:BatteryLevel? = nil
    @Published var mediaPlayerState:MediaPlayerState? = nil
    @Published var dialerState:DialerState? = nil
    
    @Published var volumeLevel:VolumeLevel? =  nil
    
    @Published var rideGridState:RideGridState? = nil
    @Published var secondPhoneState:SecondPhoneState? = nil
    @Published var rideGridDevices:Array<RideGridDevice> = Array()
    @Published var rideLynkState:RideLynkState? = nil
    @Published var rideLynkDevices:Set<Buddy> = Set()
    //    @Published var rideLynkDevices:Set<RideLynkDevice> = Set(arrayLiteral: rideLynkDevice1, rideLynkDevice2, rideLynkDevice3)
    
    
    @Published var rideGridRidersRadarMapNodeMap: [Int: String] = [:]
    @Published var rideGridRidersRadarMap: [String: RideGridDevice] = [:]
    
    
    var connectionStatecancellable:AnyCancellable?
    
    init(){
        self.startTimer()
        self.handleConnectionState()
        self.refreshRideGridGroups()
        self.refreshSpeedDialList()
    }
    
    
    private func handleConnectionState(){
        connectionStatecancellable = $connectionState.sink(receiveValue: { value in
            switch value{
            case .DeviceReady(bluArmorDevice: let bluArmorDevice):
                if bluArmorDevice.model == BluArmorModel.SX_20_OTA || bluArmorDevice.model == BluArmorModel.SX_10_OTA{
                    self.navigateTo(appDestination: .OtaDest)
                }else {
                    self.navigateTo(appDestination: .DashboardDest)
                }
                
            default:
                self.popBackTo(popDestination: .DiscoveryDest)
            }
        })
    }
    
    func startTimer(){
        DispatchQueue.main.async {
            sleep(1)
            self.navigateTo(appDestination: .DiscoveryDest)
        }
    }
    
    func rebootInOtaMode(){
        device?.otaSwitch.switchToOtaMode()
    }
    
    private func otaSwitchStateObserver(state:OtaSwitchState?){
        self.otaSwitchState = state
        
        switch state{
        case .NoUpdateAvailable:
            self.updateAvailable = false
        case .UpdateAvailable(let changeLogs):
            self.updateAvailable = true

        @unknown default:
            self.updateAvailable = false
        }
        
    }
    
    private func firmwareUpdateObserver(state:FirmwareUpdateState?){
        self.firmwareUpdateState = state
//        "\(state)".logE(tag: "Main VM")
        switch state{
        case .Upgrading(appProgress: let appProgress, ridegridProgress: let ridegridProgress):
                        self.firmwareUpdateProgress = appProgress
                        self.ridegridUpdateProgress = ridegridProgress
            
//        case .Upgrading(appProgress: let appProgress, ridegridProgress: let ridegridProgress, ridegridPart: let ridegridPart, ridegridPartCount: let ridegridPartCount):

        default:
            break
        }
        
    }

    private func deviceInfoObserver(state:DeviceDetails?){ self.deviceInfo = state }
    private func secondPhoneStateObserver(state:SecondPhoneState?){ self.secondPhoneState = state }
    private func batteryObserver(state:BatteryLevel){ self.batteryLevel = state }
    private func mediaPlayerObserver(state:MediaPlayerState){ self.mediaPlayerState = state }
    private func volumeLevelObserver(state:VolumeLevel){ self.volumeLevel = state }
    private func dialerObserver(state:DialerState){ self.dialerState = state }
    private func rideGridObserver(state:RideGridState){
        self.rideGridState = state
        switch state{
        case .OFF:
            rideGridRidersRadarMapNodeMap.removeAll()
            rideGridRidersRadarMap.removeAll()
        case .DISCOVERY:
            rideGridRidersRadarMapNodeMap.removeAll()
            rideGridRidersRadarMap.removeAll()
        default:
            break
        }
        
    }
    
    private func rideGridDevicesObserver(rideGridDevices:Set<RideGridDevice>){
        
        self.rideGridDevices = Array(rideGridDevices)
        
        let sortedList:Array<RideGridDevice> = Array(rideGridDevices.sorted { r1, r2 in
            r1.distance > r2.distance
        }[0..<min(rideGridDevices.count, 5)])
        
        for rDevice in sortedList {
            if(rDevice.bearing == 0 && rDevice.distance <= 100){
                if let key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]{
                    rideGridRidersRadarMap.removeValue(forKey: key)
                }
                
                if rideGridRidersRadarMap["T1"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T1"
                    rideGridRidersRadarMap["T1"] = rDevice
                }
                else if rideGridRidersRadarMap["T2"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T2"
                    rideGridRidersRadarMap["T2"] = rDevice
                }
                else if rideGridRidersRadarMap["T5"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T5"
                    rideGridRidersRadarMap["T5"] = rDevice
                }
                else if rideGridRidersRadarMap["T8"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T8"
                    rideGridRidersRadarMap["T8"] = rDevice
                }
                else if rideGridRidersRadarMap["T9"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T9"
                    rideGridRidersRadarMap["T9"] = rDevice
                }
            }
            else if rDevice.bearing == 0 && rDevice.distance <= 200{
                if let key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]{
                    rideGridRidersRadarMap.removeValue(forKey: key)
                }
                
                if rideGridRidersRadarMap["T8"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T8"
                    rideGridRidersRadarMap["T8"] = rDevice
                }
                else if rideGridRidersRadarMap["T9"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T9"
                    rideGridRidersRadarMap["T9"] = rDevice
                }
                else if rideGridRidersRadarMap["T10"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T10"
                    rideGridRidersRadarMap["T10"] = rDevice
                }
                else if rideGridRidersRadarMap["T11"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T11"
                    rideGridRidersRadarMap["T11"] = rDevice
                }
                else if rideGridRidersRadarMap["T12"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T12"
                    rideGridRidersRadarMap["T12"] = rDevice
                }
            }
            else if rDevice.bearing == 0 && rDevice.distance > 200 {
                if let key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]{
                    rideGridRidersRadarMap.removeValue(forKey: key)
                }
                
                if rideGridRidersRadarMap["T6"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T6"
                    rideGridRidersRadarMap["T6"] = rDevice
                }
                else if rideGridRidersRadarMap["T7"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T7"
                    rideGridRidersRadarMap["T7"] = rDevice
                }
                else if rideGridRidersRadarMap["T8"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T8"
                    rideGridRidersRadarMap["T8"] = rDevice
                }
                else if rideGridRidersRadarMap["T9"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T9"
                    rideGridRidersRadarMap["T9"] = rDevice
                }
                else if rideGridRidersRadarMap["T10"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T10"
                    rideGridRidersRadarMap["T10"] = rDevice
                }
            }
            
            else if rDevice.bearing == 45{
                if let key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]{
                    rideGridRidersRadarMap.removeValue(forKey: key)
                }
                
                if rideGridRidersRadarMap["T3"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T3"
                    rideGridRidersRadarMap["T3"] = rDevice
                }
                else if rideGridRidersRadarMap["T13"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T13"
                    rideGridRidersRadarMap["T13"] = rDevice
                }
                else if rideGridRidersRadarMap["T6"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T6"
                    rideGridRidersRadarMap["T6"] = rDevice
                }
                else if rideGridRidersRadarMap["T8"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T8"
                    rideGridRidersRadarMap["T8"] = rDevice
                }
                else if rideGridRidersRadarMap["T11"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T11"
                    rideGridRidersRadarMap["T11"] = rDevice
                }
            }
            else if rDevice.bearing == 90{
                if let key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]{
                    rideGridRidersRadarMap.removeValue(forKey: key)
                }
                
                
                if rideGridRidersRadarMap["R1"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "R1"
                    rideGridRidersRadarMap["R1"] = rDevice
                }
                else if rideGridRidersRadarMap["R2"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "R2"
                    rideGridRidersRadarMap["R2"] = rDevice
                }
                else if rideGridRidersRadarMap["R3"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "R3"
                    rideGridRidersRadarMap["R3"] = rDevice
                }
                else if rideGridRidersRadarMap["R4"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "R4"
                    rideGridRidersRadarMap["R4"] = rDevice
                }
                else if rideGridRidersRadarMap["R5"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "R5"
                    rideGridRidersRadarMap["R5"] = rDevice
                }
                else if rideGridRidersRadarMap["R6"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "R6"
                    rideGridRidersRadarMap["R6"] = rDevice
                }
            }
            else if rDevice.bearing == 135{
                if let key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]{
                    rideGridRidersRadarMap.removeValue(forKey: key)
                }
                
                if rideGridRidersRadarMap["B3"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B3"
                    rideGridRidersRadarMap["B3"] = rDevice
                }
                else if rideGridRidersRadarMap["B13"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B13"
                    rideGridRidersRadarMap["B13"] = rDevice
                }
                else if rideGridRidersRadarMap["B6"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B6"
                    rideGridRidersRadarMap["B6"] = rDevice
                }
                else if rideGridRidersRadarMap["B8"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B8"
                    rideGridRidersRadarMap["B8"] = rDevice
                }
                else if rideGridRidersRadarMap["B11"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B11"
                    rideGridRidersRadarMap["B11"] = rDevice
                }
                
            }
            
            else if rDevice.bearing == 180 && rDevice.distance <= 100{
                if let key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]{
                    rideGridRidersRadarMap.removeValue(forKey: key)
                }
                
                if rideGridRidersRadarMap["B1"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B1"
                    rideGridRidersRadarMap["B1"] = rDevice
                }
                else if rideGridRidersRadarMap["B2"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B2"
                    rideGridRidersRadarMap["B2"] = rDevice
                }
                else if rideGridRidersRadarMap["B5"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B5"
                    rideGridRidersRadarMap["B5"] = rDevice
                }
                else if rideGridRidersRadarMap["B8"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B8"
                    rideGridRidersRadarMap["B8"] = rDevice
                }
                else if rideGridRidersRadarMap["B9"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B9"
                    rideGridRidersRadarMap["B9"] = rDevice
                }
            }
            else if rDevice.bearing == 180 && rDevice.distance <= 200{
                if let key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]{
                    rideGridRidersRadarMap.removeValue(forKey: key)
                }
                
                if rideGridRidersRadarMap["B8"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B8"
                    rideGridRidersRadarMap["B8"] = rDevice
                }
                else if rideGridRidersRadarMap["B9"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B9"
                    rideGridRidersRadarMap["B9"] = rDevice
                }
                else if rideGridRidersRadarMap["B10"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B10"
                    rideGridRidersRadarMap["B10"] = rDevice
                }
                else if rideGridRidersRadarMap["B11"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B11"
                    rideGridRidersRadarMap["B11"] = rDevice
                }
                else if rideGridRidersRadarMap["B12"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B12"
                    rideGridRidersRadarMap["B12"] = rDevice
                }
                
            }
            else if rDevice.bearing == 180 && rDevice.distance > 200{
                if let key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]{
                    rideGridRidersRadarMap.removeValue(forKey: key)
                }
                
                if rideGridRidersRadarMap["B6"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B6"
                    rideGridRidersRadarMap["B6"] = rDevice
                }
                else if rideGridRidersRadarMap["B7"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B7"
                    rideGridRidersRadarMap["B7"] = rDevice
                }
                else if rideGridRidersRadarMap["B8"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B8"
                    rideGridRidersRadarMap["B8"] = rDevice
                }
                else if rideGridRidersRadarMap["B9"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B9"
                    rideGridRidersRadarMap["B9"] = rDevice
                }
                else if rideGridRidersRadarMap["B10"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B10"
                    rideGridRidersRadarMap["B10"] = rDevice
                }
            }
            
            else if rDevice.bearing == -135{
                if let key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]{
                    rideGridRidersRadarMap.removeValue(forKey: key)
                }
                
                if rideGridRidersRadarMap["B4"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B4"
                    rideGridRidersRadarMap["B4"] = rDevice
                }
                else if rideGridRidersRadarMap["B14"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B14"
                    rideGridRidersRadarMap["B14"] = rDevice
                }
                else if rideGridRidersRadarMap["B7"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B7"
                    rideGridRidersRadarMap["B7"] = rDevice
                }
                else if rideGridRidersRadarMap["B9"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B9"
                    rideGridRidersRadarMap["B9"] = rDevice
                }
                else if rideGridRidersRadarMap["B12"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "B12"
                    rideGridRidersRadarMap["B12"] = rDevice
                }
            }
            else if rDevice.bearing == -90{
                if let key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]{
                    rideGridRidersRadarMap.removeValue(forKey: key)
                }
                
                if rideGridRidersRadarMap["L1"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "L1"
                    rideGridRidersRadarMap["L1"] = rDevice
                }
                else if rideGridRidersRadarMap["L2"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "L2"
                    rideGridRidersRadarMap["L2"] = rDevice
                }
                else if rideGridRidersRadarMap["L3"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "L3"
                    rideGridRidersRadarMap["L3"] = rDevice
                }
                else if rideGridRidersRadarMap["L4"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "L4"
                    rideGridRidersRadarMap["L4"] = rDevice
                }
                else if rideGridRidersRadarMap["L5"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "L5"
                    rideGridRidersRadarMap["L5"] = rDevice
                }
                else if rideGridRidersRadarMap["L6"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "L6"
                    rideGridRidersRadarMap["L6"] = rDevice
                }
            }
            else if rDevice.bearing == -45{
                if let key = rideGridRidersRadarMapNodeMap[rDevice.nodeId]{
                    rideGridRidersRadarMap.removeValue(forKey: key)
                }
                
                if rideGridRidersRadarMap["T4"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T4"
                    rideGridRidersRadarMap["T4"] = rDevice
                }
                else if rideGridRidersRadarMap["T14"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T14"
                    rideGridRidersRadarMap["T14"] = rDevice
                }
                else if rideGridRidersRadarMap["T7"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T7"
                    rideGridRidersRadarMap["T7"] = rDevice
                }
                else if rideGridRidersRadarMap["T9"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T9"
                    rideGridRidersRadarMap["T9"] = rDevice
                }
                else if rideGridRidersRadarMap["T12"] == nil{
                    rideGridRidersRadarMapNodeMap[rDevice.nodeId] = "T12"
                    rideGridRidersRadarMap["T12"] = rDevice
                }
                
            }
        }
        
        let map = rideGridRidersRadarMap.map { k,v in
            "\(k): \(v.name)"
        }.joined(separator: ", ")
        
        let map2 = sortedList.map { device in
            "\(device.name): \(device.bearing): \(device.distance)"
        }.joined(separator: ", ")
        
//        "MAP: \(map) || \(map2)".logD(tag: "RIDEGRID")
    }
    
    private func rideLynkObserver(state:RideLynkState){
        self.rideLynkState = state
        
        if case RideLynkState.Discovery(let buddies) = state {
            self.rideLynkDevices = self.rideLynkDevices.union(Set(buddies))
        }
        
        //        if case RideLynkState.Discovery(let rideLynkDevice) = state, let device = rideLynkDevice{
        //            "COUNTER \(self.rideLynkDevices.count)".logD
        //        }
        
        
        
    }
    
    private func onConnectionStateChange(state:ConnectionState){
        self.connectionState = state
    }
    
    private func onDeviceReady(device:ConnectedBluArmorDevice){
//        "\(#function)".logE
        
        switch device.model{
            
        case .SX_10:
            initSXDevice(device: device, withRideGrid: false)
        case .SX_20:
            initSXDevice(device: device)
        case .SX_10_OTA:
            initSxOtaDevice(device: device)
        case .SX_20_OTA:
            initSxOtaDevice(device: device)
        default:
            break
        }
        
    }
    
    private func initSxOtaDevice(device:ConnectedBluArmorDevice){
        self.deviceName = device.deviceName
        self.deviceModel = device.model
        self.otaDevice = device as! S20XOta
        self.otaDevice?.firmwareUpdate.observe(onUpdate: self.firmwareUpdateObserver(state:))
    }
    
    
    private func initSXDevice(device:ConnectedBluArmorDevice, withRideGrid:Bool = true){
        self.deviceName = device.deviceName
        self.deviceModel = device.model
        self.device = device as! S20X
        self.device?.otaSwitch.observe(onUpdate: self.otaSwitchStateObserver(state:))
        self.device?.deviceInfo.observe(onUpdate: self.deviceInfoObserver(state:))
        self.device?.battery.observe(onUpdate: self.batteryObserver(state:))
        self.device?.mediaPlayer.observe(onUpdate: self.mediaPlayerObserver(state:))
        self.device?.dialer.observe(onUpdate: self.dialerObserver(state:))
        self.device?.volume.observe(onUpdate: self.volumeLevelObserver(state:))
        self.device?.rideLynk.observe(onUpdate: self.rideLynkObserver(state:))
        self.device?.secondPhone.observe(onUpdate: self.secondPhoneStateObserver(state:))
        if withRideGrid{
            self.device?.rideGrid.observe(onUpdate: self.rideGridObserver(state:))
            self.device?.rideGrid.nearbyRiders.observe(onUpdate: self.rideGridDevicesObserver(rideGridDevices:))}
    }
    
    func disconnectSecondPhone(){ device?.secondPhone.disconnect()}
    func startSecondPhoneAdvertising(){device?.secondPhone.startAdvertising()}
    func stopSecondPhoneAdvertising(){ device?.secondPhone.stopAdvertising()}
    
    func volumeControlAction(action:VolumeControlAction){
        switch action {
        case .ToggleDynamicVolume(toggleOn: let toggleOn):
            if toggleOn{ self.device?.volume.enableDynamicMode() }
            else { self.device?.volume.disableDynamicMode() }
        case .ChangeVolume(level: let level):
            if let volumeLevel = volumeLevel {
                //                self.volumeLevel = VolumeLevel(level: level,
                //                                               dynamicMode: volumeLevel.dynamicMode,
                //                                               volumeBoost: volumeLevel.volumeBoost,
                //                                               equalizerBassBoost: volumeLevel.equalizerBassBoost)
            }
            
            self.device?.volume.changeVolume(level: level)
        case .ChangeVolumeBoost(volumeBoost: let volumeBoost):
            self.device?.volume.changeVolumeBoost(volumeBoost: volumeBoost)
        case .ChangeEqualizerBoost(equalizerBassBoost: let equalizerBassBoost):
            self.device?.volume.changeEqualizerBassBoost(equalizerBassBoost: equalizerBassBoost)
        }
    }
    
    func musicAction(action:MusicActions){
        switch action {
        case .PLAY:
            self.device?.mediaPlayer.playMusic()
        case .PAUSE:
            self.device?.mediaPlayer.pauseMusic()
        case .NEXT:
            self.device?.mediaPlayer.nextMusic()
        case .PREVIOUS:
            self.device?.mediaPlayer.previousMusic()
        }
    }
    
    func dialerAction(action:DialerActions){
        switch action {
        case .ANSWER:
            self.device?.dialer.acceptPhoneCall()
        case .REJECT:
            self.device?.dialer.rejectPhoneCall()
        case .END:
            self.device?.dialer.endPhoneCall()
        }
    }
    
    func rideLynkAction(action:RideLynkActions){
        switch action {
        case .StartDiscovery:
            self.rideLynkDevices.removeAll()
            self.device?.rideLynk.startDiscovery()
        case .StopDiscovery: self.device?.rideLynk.stopDiscovery()
        case .Disconnect: self.device?.rideLynk.disconnect()
        case .Connect(device: let device): self.device?.rideLynk.connect(buddy:device )
        case .StartCall: self.device?.rideLynk.startCall()
        case .StopCall: self.device?.rideLynk.endCall()
        }
    }
    
    func rideGridAction(action:RideGridActions){
        switch action {
        case .StartCall:
            self.device?.rideGrid.startRideGridCall()
        case .StopCall:
            self.device?.rideGrid.stopRideGridCall()
        case .Mute:
            self.device?.rideGrid.muteCall()
        case .UnMute:
            self.device?.rideGrid.unMuteCall()
        case .TurnOn:
            self.device?.rideGrid.turnOn()
        case .TurnOff:
            self.device?.rideGrid.turnOff()
        }
    }
    
    deinit {
        connectionStatecancellable?.cancel()
    }
    
    func getBuddyInviteQrCodeData()->String?{
        if let buddy = self.device?.buddiesDataSource.getActiveBuddy(){
            return self.device?.buddiesDataSource.encodeToQrCodeData(buddy: buddy)
        }
        return nil
    }
    
    
    
    func getActiveBuddy()->Buddy?{
        return self.device?.buddiesDataSource.getActiveBuddy()
    }
    func getRidegridGroupInviteQrCodeData(rideGridGroup: RideGridGroup)->String?{
        return self.device?.rideGridGroupDataSource.encodeToQrCodeData(ridegridGroup: rideGridGroup)
    }
    
    // RIGEGRID View
    @Published var inviteRideGridGroup: RideGridGroup? = nil
    
    @Published var ridegridGroups: [RideGridGroup] = []
    @Published var ridegridEditMode: Bool = false
    @Published var newRideGridGroupName: String = ""
    @Published var newRideGridGroupPrimary: Bool = false
    
    func rideGridGroupToInviteLink(rideGridGroup:RideGridGroup)->String{
        let data = "https://thebluarmor.com/" + (rideGridGroup.inviteCode?.addingPercentEncoding(withAllowedCharacters: .alphanumerics) ?? "")
        return  data
    }
    
    func buddyInviteLink(buddy:Buddy)->String{
        let data = "https://thebluarmor.com/" + (buddy.id.addingPercentEncoding(withAllowedCharacters: .alphanumerics) ?? "") + "/" + (buddy.name.addingPercentEncoding(withAllowedCharacters: .alphanumerics) ?? "")
        return  data
    }
    
    func setInviteRideGridGroup(rideGridGroup:RideGridGroup){
        self.inviteRideGridGroup = rideGridGroup
    }
    
    func refreshRideGridGroups(){
        self.ridegridGroups = self.bluConnectManager.rideGridGroupDataSource.getAllGroups()
    }
    
    func createRideGridGroup(){
        if let group = self.device?.rideGrid.create(ridegridGroupName: self.newRideGridGroupName, isPrimary: self.newRideGridGroupPrimary){
            self.refreshRideGridGroups()
            self.newRideGridGroupName = ""
            self.newRideGridGroupPrimary = false
            self.navigateToInviteRiders(groupUniqueId: group.id)
        }
    }
    
    func deleteRidegridGroup(rideGridGroup:RideGridGroup){
        _ = self.bluConnectManager.rideGridGroupDataSource.delete(ridegridGroup: rideGridGroup)
        if (rideGridGroup.active){
            
        }
        self.refreshRideGridGroups()
    }
    
    func markRideGridGroupPrimary(rideGridGroup:RideGridGroup){
        self.device?.rideGrid.setActiveGroup(ridegridGroup: rideGridGroup)
//        "\(#function) \(rideGridGroup)".logE(tag: "RIDEGRID")
        self.refreshRideGridGroups()
    }
    
    func navigateToInviteRiders(groupUniqueId:String){
        self.inviteRideGridGroup = self.bluConnectManager.rideGridGroupDataSource.getRideGridGroupByGroupId(rideGridGroupId: groupUniqueId)
        if self.inviteRideGridGroup != nil{
            self.navigateTo(appDestination: .RideGridInviteRidersDest, withBackStack: false)
        }
    }
    
    func navigateToRideGridDashboard(rideGridGroup:RideGridGroup){
        self.inviteRideGridGroup = self.bluConnectManager.rideGridGroupDataSource.getRideGridGroupByGroupId(rideGridGroupId: rideGridGroup.id)
        
        if (self.inviteRideGridGroup != nil || !rideGridGroup.privateGroup){
            self.navigateTo(appDestination: .RideGridDashboardDest)
        }
    }
    
    func navigateToRideGridCreateGroup(){
        self.rideGridGroupScanResult = nil
        if (self.rideGridState == RideGridState.OFF){
            self.device?.rideGrid.turnOn()
        }
        self.navigateTo(appDestination: .RideGridCreateGroupDest)
    }
    
    
    // RIGEGRID View END
    
    // Speed dial
    
    
    func refreshBuddyList() {
        //        self.buddyDataSource.getAllContacts().forEach{b in
        ////            "$$$$$$$$ : \(b.macAddress)".logE
        //        }
        self.buddyList = self.buddyList.union(Set(self.bluConnectManager.buddiesDataSource.getAllBuddies()))
    }
    
    func navigateToSpeedDialEditView() {
        navigateTo(appDestination: .SpeedDialEditDest)
    }
    
    func navigateToAddBuddiesInviteView() {
        navigateTo(appDestination: .AddBuddiesDest)
    }
    
    @Published var buddyList: Set<Buddy> = Set()
    
    @Published var speedDialDevices: Array<Buddy?> = Array(arrayLiteral: nil,nil,nil,nil,nil)
    @Published var newSpeedDialList: Set<Buddy> = Set()
    
    func connectSpeedDialDevice(speedDialDevice: Buddy) {
        self.device?.speedDial.dial(buddy: speedDialDevice)
    }
    
    func refreshSpeedDialList() {
        self.speedDialDevices = self.bluConnectManager.speedDialContactDataSource.getAllContacts()
        
        self.speedDialDevices.forEach{ device in if let device = device{ newSpeedDialList.insert(device) } }
    }
    
    func addBuddyToSpeedDial(buddy: Buddy) {
        self.newSpeedDialList.insert(buddy)
    }
    
    func removeSpeedDialDevice(speedDialDevice: Buddy) {
        if let device = self.newSpeedDialList.first(where: { device in
            device.macAddress.lowercased() == speedDialDevice.macAddress.lowercased()
        }){
            self.newSpeedDialList.remove(device)
        }
    }
    
    func saveSpeedDialList() {
        _ = self.bluConnectManager.speedDialContactDataSource.updateSpeedDialList(speedDialDevices: Array(self.newSpeedDialList))
        self.refreshSpeedDialList()
        self.popBack()
    }
    
    //    @Published var speedDialDevices:Array<SpeedDialDevice?> = Array(arrayLiteral: nil,nil,nil,nil,nil)
    
    
    // Speed dial END
    
    
    // QrCode Scanner
    
    @Published var rideGridGroupScanResult: RideGridGroupInvitation? = nil
    
    let scanInterval: Double = 1.0
    
    @Published var torchIsOn: Bool = false
    @Published var lastQrCode: String = "Qr-code goes here"
    
    func clearRideGridGroupScanResult(){
        self.newRideGridGroupName = ""
        self.newRideGridGroupPrimary = false
        self.rideGridGroupScanResult = nil
        self.lastQrCode = ""
    }
    
    
    func createRideGridGroupFromScan(){
        
        if let rideGridGroupScanResult = self.rideGridGroupScanResult {
            //            let rideGridGroup = self.bluConnectManager.rideGridGroupDataSource.join(rideGridGroupQrData: rideGridGroupScanResult)
            
            let rideGridGroup = self.device?.rideGrid.join(rideGridGroupInvitation: rideGridGroupScanResult, isPrimary: self.newRideGridGroupPrimary)
            
            //            let rideGridGroup = self.device?.rideGrid. (rideGridGroupQrData: rideGridGroupScanResult, isPrimary: self.newRideGridGroupPrimary)
            
            //            if let _ = rideGridGroup, self.newRideGridGroupPrimary == true{
            //                _ = self.bluConnectManager.rideGridGroupDataSource.removeActiveGroup()
            //                _ = self.bluConnectManager.rideGridGroupDataSource.setActiveGroup(ridegridGroup: rideGridGroupScanResult.ridegridGroup)
            //            }
            
            self.refreshRideGridGroups()
            self.newRideGridGroupName = ""
            self.newRideGridGroupPrimary = false
            self.lastQrCode = ""
            self.navigateToInviteRiders(groupUniqueId: rideGridGroupScanResult.ridegridGroup.id)
            
        }
        
    }
    
    func onFoundQrCode(_ code: String) {
        if self.lastQrCode == code { return }
        self.lastQrCode = code
        
//        "\(code)".logE(tag: "QR-CODE")
        
        if let rideGridGroupQrData = RideGridGroupInvitation.decodeFromQrCodeData(qrCodeData: code){
            rideGridGroupScanResult = rideGridGroupQrData
        }
        
        //        "\(#function) \(matches(for: "^https://thebluarmor\\.com/(\\d+)/(.+)", in: code)) , \(code.split(separator: "/"))".logE
        //
        //        if !matches(for: "^https://thebluarmor\\.com/(\\d+)/(.+)", in: code).isEmpty{
        //            let data = code.split(separator: "/")
        //            //            rideGridGroupScanResult = RideGridGroup(groupName: String(data[3]).replacingOccurrences(of: "+", with: "%20"), id: Int64(data[2]) ?? -1, primary: false, createTimestamp:  Int64(NSDate().timeIntervalSince1970))
        //        }
    }
    
    
    func onBuddiedQrCodeDataScan(_ code: String) {
        if self.lastQrCode == code { return }
        self.lastQrCode = code
        
//        "\(code)".logE(tag: "QR-CODE")
        
        if let buddy = Buddy.decodeFromQrCodeData(qrData: code){
            device?.buddiesDataSource.add(buddy: buddy)
            refreshBuddyList()
            popBack()
        }
        
        //        "\(#function) \(matches(for: "^https://thebluarmor\\.com/(\\d+)/(.+)", in: code)) , \(code.split(separator: "/"))".logE
        //
        //        if !matches(for: "^https://thebluarmor\\.com/(\\d+)/(.+)", in: code).isEmpty{
        //            let data = code.split(separator: "/")
        //            //            rideGridGroupScanResult = RideGridGroup(groupName: String(data[3]).replacingOccurrences(of: "+", with: "%20"), id: Int64(data[2]) ?? -1, primary: false, createTimestamp:  Int64(NSDate().timeIntervalSince1970))
        //        }
    }
    
    
    
    
    @Published var audioOverlay: Bool = false{
        didSet{
            
            device?.rideGrid.changeMusicOverlayLevel(enable: audioOverlay)
//            switch audioOverlay{
//            case true:
//
//                device?.rideGrid.changeMusicOverlay(audioOverlayState: .ON(level: Int(audioOverlayLevel)))
//            case false:
//                device?.rideGrid.changeMusicOverlay(audioOverlayState: .OFF(level: Int(audioOverlayLevel)))
//            }
        }
    }
    
    @Published var whatsAppAutoRead: Bool = false{
        didSet{
            bluConnectManager.sdkPrefDataSource.setWhatsappAutoRead(enable: whatsAppAutoRead )
        }
    }
    
    
    
    
    @Published var phoneAutoAnswerAll: Bool = false
    @Published var phoneAutoAnswerKnown: Bool = false
    @Published var phoneAutoAnswerManual: Bool = true
    
    @Published var phoneAutoRejectAll: Bool = false
    @Published var phoneAutoRejectUnknown: Bool = false
    @Published var phoneAutoRejectManual: Bool = true
    
    
    
    @Published var phoneAutoReject: PhoneCallAutoRejectRadioOption = .AUTO_REJECT_MANUALLY
    @Published var phoneAutoAnswer: PhoneCallAutoAnswerRadioOption = .AUTO_ANSWER_MANUALLY
    private var phoneCallPreferenceOption:PhoneCallPreferenceOption = .MANUAL
    
    func refreshPreferences(){
        switch bluConnectManager.sdkPrefDataSource.getPhoneCallPreference() {
        case .MANUAL:
            phoneAutoAnswerChange(phoneCallAutoAnswerRadioOption: .AUTO_ANSWER_MANUALLY)
            phoneAutoRejectChange(phoneCallAutoRejectRadioOption: .AUTO_REJECT_MANUALLY)
            break
        case .AUTO_ANSWER_ALL:
            phoneAutoAnswerChange(phoneCallAutoAnswerRadioOption: .AUTO_ANSWER_ALL)
            break
        case .AUTO_REJECT_ALL:
            phoneAutoRejectChange(phoneCallAutoRejectRadioOption: .AUTO_REJECT_ALL)
            break
        case .AUTO_ANSWER_KNOWN:
            phoneAutoAnswerChange(phoneCallAutoAnswerRadioOption: .AUTO_ANSWER_KNOWN)
            
            break
        case .AUTO_REJECT_UNKNOWN:
            phoneAutoRejectChange(phoneCallAutoRejectRadioOption: .AUTO_REJECT_UNKNOWN)
            break
        case .AUTO_ANSWER_KNOWN_AND_AUTO_REJECT_UNKNOWN:
            phoneAutoAnswerChange(phoneCallAutoAnswerRadioOption: .AUTO_ANSWER_KNOWN)
            phoneAutoRejectChange(phoneCallAutoRejectRadioOption: .AUTO_REJECT_UNKNOWN)
            break
        @unknown default:
            break
        }
        
        
        
        self.whatsAppAutoRead = bluConnectManager.sdkPrefDataSource.getWhatsappAutoRead()
        
        
        switch bluConnectManager.sdkPrefDataSource.getVolumeBoostPreference(){
        case .EASY_ON_EAR:
            volumeBoostChange(volumeBoostRadioOption: .EASE_ON_EARS)
        case .BALANCED:
            volumeBoostChange(volumeBoostRadioOption: .BALANCED)
        case .HIGH_OUTPUT:
            volumeBoostChange(volumeBoostRadioOption: .HIGH_OUTPUT)
        @unknown default:
            volumeBoostChange(volumeBoostRadioOption: .EASE_ON_EARS)
        }
        
        switch bluConnectManager.sdkPrefDataSource.getEqualizerPreferenceOption(){
        case EqualizerPreferenceOption.REGULAR:
            equalizerBoostChange(equalizerBoostRadioOption: .REGULAR)
        case EqualizerPreferenceOption.BASS_BOOST:
            equalizerBoostChange(equalizerBoostRadioOption: .BASS_BOOST)
        }
        
        switch bluConnectManager.sdkPrefDataSource.getVoicePromptLevel(){
        case .IMPORTANT:
            voicePromptLevel1 = true
            voicePromptLevel2 = false
        case .ALL:
            voicePromptLevel1 = false
            voicePromptLevel2 = true
        }
        
        
        
        
        switch bluConnectManager.sdkPrefDataSource.getAudioWeaveMusicDucking(){
        case true:
            audioOverlay = true
            audioOverlayLevel = Float(bluConnectManager.sdkPrefDataSource.getAudioWeaveMusicDuckingLevel())
            break
        case false:
            audioOverlay = false
            audioOverlayLevel = Float(bluConnectManager.sdkPrefDataSource.getAudioWeaveMusicDuckingLevel())
            break
        }
    }
    
    private func persistPhoneOptions(){
        if phoneAutoAnswer == .AUTO_ANSWER_ALL {
            bluConnectManager.sdkPrefDataSource.setPhoneCallPreference(phoneCallPreferenceOption: .AUTO_ANSWER_ALL)
        } else if phoneAutoReject == .AUTO_REJECT_ALL {
            bluConnectManager.sdkPrefDataSource.setPhoneCallPreference(phoneCallPreferenceOption: .AUTO_REJECT_ALL)
        } else if phoneAutoAnswer == .AUTO_ANSWER_KNOWN &&  phoneAutoReject == .AUTO_REJECT_UNKNOWN {
            bluConnectManager.sdkPrefDataSource.setPhoneCallPreference(phoneCallPreferenceOption: .AUTO_ANSWER_KNOWN_AND_AUTO_REJECT_UNKNOWN)
        } else if phoneAutoAnswer == .AUTO_ANSWER_KNOWN {
            bluConnectManager.sdkPrefDataSource.setPhoneCallPreference(phoneCallPreferenceOption: .AUTO_ANSWER_KNOWN)
        } else if phoneAutoReject == .AUTO_REJECT_UNKNOWN {
            bluConnectManager.sdkPrefDataSource.setPhoneCallPreference(phoneCallPreferenceOption: .AUTO_REJECT_UNKNOWN)
        } else {
            bluConnectManager.sdkPrefDataSource.setPhoneCallPreference(phoneCallPreferenceOption: .MANUAL)
        }
    }
    
    func phoneAutoAnswerChange(phoneCallAutoAnswerRadioOption: PhoneCallAutoAnswerRadioOption){
        
        phoneAutoAnswerAll = false
        phoneAutoAnswerKnown = false
        phoneAutoAnswerManual = false
        phoneAutoAnswer = phoneCallAutoAnswerRadioOption
        
        switch phoneAutoAnswer {
        case .AUTO_ANSWER_ALL:
            phoneAutoAnswerAll = true
            if phoneAutoReject != .AUTO_REJECT_MANUALLY{
                phoneAutoRejectChange(phoneCallAutoRejectRadioOption: .AUTO_REJECT_MANUALLY)
            }
        case .AUTO_ANSWER_KNOWN:
            phoneAutoAnswerKnown = true
            if phoneAutoReject == .AUTO_REJECT_ALL{
                phoneAutoRejectChange(phoneCallAutoRejectRadioOption: .AUTO_REJECT_MANUALLY)
            }
        case .AUTO_ANSWER_MANUALLY:
            phoneAutoAnswerManual = true
        }
        
        persistPhoneOptions()
    }
    
    
    
    func phoneAutoRejectChange(phoneCallAutoRejectRadioOption: PhoneCallAutoRejectRadioOption){
        phoneAutoRejectAll = false
        phoneAutoRejectUnknown = false
        phoneAutoRejectManual = false
        phoneAutoReject = phoneCallAutoRejectRadioOption
        
        switch phoneAutoReject {
        case .AUTO_REJECT_ALL:
            phoneAutoRejectAll = true
            if phoneAutoAnswer != .AUTO_ANSWER_MANUALLY {
                phoneAutoAnswerChange(phoneCallAutoAnswerRadioOption: .AUTO_ANSWER_MANUALLY)
            }
        case .AUTO_REJECT_UNKNOWN:
            phoneAutoRejectUnknown = true
            if phoneAutoAnswer == .AUTO_ANSWER_ALL {
                phoneAutoAnswerChange(phoneCallAutoAnswerRadioOption: .AUTO_ANSWER_MANUALLY)
            }
        case .AUTO_REJECT_MANUALLY:
            phoneAutoRejectManual = true
        }
        
        persistPhoneOptions()
        
    }
    
    
    @Published var voicePromptLevel1 = false
    @Published var voicePromptLevel2 = true
    
    
    @Published var volumeBoostEasyOnEar = false
    @Published var volumeBoostBalanced = true
    @Published var volumeBoostHighOutput = false
    
    
    @Published var equalizerBoostNormal = true
    @Published var equalizerBassBoost = false
    
    @Published var volumeBoost:VolumeBoostRadioOption = .BALANCED
    @Published var equalizerBoost: EqualizerBoostRadioOption = .REGULAR
    //    @Published var voicePromptLevel: VoicePromptLevel
    @Published var audioOverlayLevel: Float = 4.0
//    @Published var audioOverlayFlag: Bool = true
    
    func volumeBoostChange(volumeBoostRadioOption: VolumeBoostRadioOption){
        volumeBoostEasyOnEar = false
        volumeBoostBalanced = false
        volumeBoostHighOutput = false
        
        volumeBoost = volumeBoostRadioOption
        
        switch volumeBoost {
        case .EASE_ON_EARS:
            volumeBoostEasyOnEar = true
            device?.volume.changeVolumeBoost(volumeBoost: .EASY_ON_EARS)
            //            bluConnectManager.sdkPrefDataSource.setVolumeBoostPreference(volumeBoostPreferenceOption: .EASY_ON_EAR)
        case .BALANCED:
            volumeBoostBalanced = true
            device?.volume.changeVolumeBoost(volumeBoost: .BALANCED)
            //            bluConnectManager.sdkPrefDataSource.setVolumeBoostPreference(volumeBoostPreferenceOption: .BALANCED)
        case .HIGH_OUTPUT:
            volumeBoostHighOutput = true
            device?.volume.changeVolumeBoost(volumeBoost: .HIGH_OUTPUT)
            //            bluConnectManager.sdkPrefDataSource.setVolumeBoostPreference(volumeBoostPreferenceOption: .HIGH_OUTPUT)
        }
    }
    
    func equalizerBoostChange(equalizerBoostRadioOption: EqualizerBoostRadioOption){
        equalizerBoostNormal = false
        equalizerBassBoost = false
        equalizerBoost = equalizerBoostRadioOption
        
        switch equalizerBoost {
        case .REGULAR:
            equalizerBoostNormal = true
            device?.volume.changeEqualizerBassBoost(equalizerBassBoost: .REGULAR)
            //            bluConnectManager.sdkPrefDataSource.setEqualizerPreferenceOption(equalizerPreferenceOption: .REGULAR)
        case .BASS_BOOST:
            equalizerBassBoost = true
            device?.volume.changeEqualizerBassBoost(equalizerBassBoost: .BASS_BOOST)
            //            bluConnectManager.sdkPrefDataSource.setEqualizerPreferenceOption(equalizerPreferenceOption: .BASS_BOOST)
        }
    }
    
    
    func audioOverlayLevelChange(value: Float){
        audioOverlayLevel = value
        device?.rideGrid.changeMusicOverlayLevel(level: Int(value))
        
        
//        switch audioOverlay{
//        case true:
//            device?.rideGrid.changeMusicOverlay(audioOverlayState: .ON(level: Int(value)))
//        case false:
//            device?.rideGrid.changeMusicOverlay(audioOverlayState: .OFF(level: Int(value)))
//        }
        
    }
    
    
    @Published var renameToggle: Bool = false
    @Published var newDeviceName: String = ""
    
    
    func rename(){
        device?.rename(name: newDeviceName)
        newDeviceName = ""
    }
    
    func changeVoicePromptLevel(voicePromptLevel: VoicePromptVerbosityLevel){
        switch voicePromptLevel{
        case .IMPORTANT:
            device?.volume.changeVoicePromptVerbosityLevel(promptVerbosityLevel: VoicePromptVerbosityLevel.IMPORTANT)
            voicePromptLevel1 = true
            voicePromptLevel2 = false
        case .ALL:
            device?.volume.changeVoicePromptVerbosityLevel(promptVerbosityLevel: VoicePromptVerbosityLevel.ALL)
            voicePromptLevel1 = false
            voicePromptLevel2 = true
        }
        
    }
    
    
    
    // END
}



enum PhoneCallAutoAnswerRadioOption : Hashable,CaseIterable {
    case AUTO_ANSWER_ALL,
         AUTO_ANSWER_KNOWN,
         AUTO_ANSWER_MANUALLY
}

enum PhoneCallAutoRejectRadioOption: Hashable {
    case AUTO_REJECT_ALL,
         AUTO_REJECT_UNKNOWN,
         AUTO_REJECT_MANUALLY
}

enum VolumeBoostRadioOption: Hashable {
    case EASE_ON_EARS,
         BALANCED,
         HIGH_OUTPUT
}

enum EqualizerBoostRadioOption: Hashable {
    case REGULAR,
         BASS_BOOST
}





enum MusicActions: Hashable{
    case PLAY, PAUSE, NEXT, PREVIOUS
}

enum DialerActions: Hashable{
    case ANSWER, REJECT, END
}

enum RideLynkActions: Hashable{
    case StartDiscovery, StopDiscovery, Disconnect, StartCall, StopCall
    case Connect(device:Buddy)
}

enum RideGridActions: Hashable{
    case StartCall, StopCall,Mute,UnMute,TurnOn,TurnOff
}

enum VolumeControlAction: Hashable{
    case ToggleDynamicVolume(toggleOn:Bool)
    case ChangeVolume(level:Int)
    case ChangeVolumeBoost(volumeBoost:VolumeBoost)
    case ChangeEqualizerBoost(equalizerBassBoost:EqualizerBassBoost)
}

func matches(for regex: String, in text: String) -> [String] {
    do {
        let regex = try NSRegularExpression(pattern: regex)
        let results = regex.matches(in: text,
                                    range: NSRange(text.startIndex..., in: text))
        return results.map {
            String(text[Range($0.range, in: text)!])
        }
    } catch let error {
        print("invalid regex: \(error.localizedDescription)")
        return []
    }
}

