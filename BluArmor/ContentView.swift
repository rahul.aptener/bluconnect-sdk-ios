//
//  ContentView.swift
//  BluArmor
//
//  Created by Rahul Gaur on 17/03/22.
//

import SwiftUI
import CoreData
import NavigationStack
import Combine

struct ContentView: View {
    @Environment(\.managedObjectContext) private var viewContext
    @EnvironmentObject private var navigationStack: NavigationStackCompat
    @ObservedObject private var vm:BluArmorAppViewModel = BluArmorAppViewModel()
    
    var body: some View {
        ZStack{}
            .onAppear(){
                if(vm.myRouter == nil){
                    vm.myRouter = MyRouter(navStack: navigationStack,vm: self.vm)
//                    vm.startTimer()
                }
            }.navigationBarBackButtonHidden(true)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
    }
}
