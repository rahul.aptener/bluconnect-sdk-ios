//
//  AppDestination.swift
//  BluArmor
//
//  Created by Rahul Gaur on 15/03/22.
//

import Foundation
import NavigationStack
import SwiftUI

class MyRouter {
    @ObservedObject private var vm:BluArmorAppViewModel
    
    private let navigationStack: NavigationStackCompat
    
    init(navStack: NavigationStackCompat,vm:BluArmorAppViewModel) {
        self.vm = vm
        self.navigationStack = navStack
        navigateTo(destination: .LandingDest)
    }
    
    func pop(){
        self.navigationStack.pop()
    }
    
    func navigateTo(destination:AppDestination) {
        
//        self.navigationStack.push(AddBuddiesView(vm: self.vm).navigationBarBackButtonHidden(true), withId: AppDestination.LandingDest.identifier)
//        
//        return
        
        if (self.navigationStack.containsView(withId: destination.identifier)){
            self.navigationStack.pop(to: .view(withId: destination.identifier))
        }else{
            switch(destination){
                
            case .LandingDest:
                self.navigationStack.push(LandingView().navigationBarBackButtonHidden(true), withId: AppDestination.LandingDest.identifier)
                
            case .DiscoveryDest:
                self.navigationStack.push(DiscoveryView(vm: self.vm).navigationBarBackButtonHidden(true), withId: AppDestination.DiscoveryDest.identifier)
                
            case .DashboardDest:
                self.navigationStack.push(DashboardView(vm: self.vm).navigationBarBackButtonHidden(true), withId: AppDestination.DashboardDest.identifier)
                
            case .PreferenceDest:
                self.navigationStack.push(PreferenceView(vm: self.vm).navigationBarBackButtonHidden(true), withId: AppDestination.PreferenceDest.identifier)
                
            case .PermissionDest:
                self.navigationStack.push(LandingView().navigationBarBackButtonHidden(true), withId: AppDestination.PermissionDest.identifier)
                
            case .OtaDest:
                self.navigationStack.push(OtaView(vm: self.vm).navigationBarBackButtonHidden(true), withId: AppDestination.OtaDest.identifier)
                
            case .RideLynkDiscoveryDest:
                self.navigationStack.push(RideLynkDiscoveryView(vm: self.vm).navigationBarBackButtonHidden(true), withId: AppDestination.RideLynkDiscoveryDest.identifier)
                
            case .RideGridGroupsDest:
                self.navigationStack.push(RidegridGroupsView(vm: self.vm).navigationBarBackButtonHidden(true), withId: AppDestination.RideGridGroupsDest.identifier)
                
            case .RideGridCreateGroupDest:
                self.navigationStack.push(RidegridCreateGroupView(vm: self.vm).navigationBarBackButtonHidden(true), withId: AppDestination.RideGridCreateGroupDest.identifier)
                
            case .RideGridDashboardDest:
                self.navigationStack.push(RideGridDashboardView(vm: self.vm).navigationBarBackButtonHidden(true), withId: AppDestination.RideGridDashboardDest.identifier)
                
            case .RideGridInviteRidersDest:
                self.navigationStack.push(RideGridShareInviteView(vm: self.vm).navigationBarBackButtonHidden(true), withId: AppDestination.RideGridInviteRidersDest.identifier)
                
            case .BuddyListDest:
                self.navigationStack.push(BuddyListView(vm: self.vm).navigationBarBackButtonHidden(true), withId: AppDestination.BuddyListDest.identifier)
                
            case .AddBuddiesDest:
                self.navigationStack.push(AddBuddiesView(vm: self.vm).navigationBarBackButtonHidden(true), withId: AppDestination.AddBuddiesDest.identifier)
                
            case .SpeedDialEditDest:
                self.navigationStack.push(SpeedDialEditView(vm: self.vm).navigationBarBackButtonHidden(true), withId: AppDestination.SpeedDialEditDest.identifier)
            }
        }
    }
}

enum AppDestination: Equatable{
    
    case LandingDest
    
    case DiscoveryDest
    
    case DashboardDest
    
    case PreferenceDest
    
    case PermissionDest
    
    case OtaDest
    
    case RideLynkDiscoveryDest
    
    case RideGridGroupsDest
    
    case RideGridCreateGroupDest
    
    case RideGridDashboardDest
    
    case RideGridInviteRidersDest
    
    case BuddyListDest
    
    case AddBuddiesDest

    case SpeedDialEditDest
    
    public var identifier: String{
        switch self {
            
        case .LandingDest: return "app_landing"
            
        case .DiscoveryDest: return "app_discovery"
            
        case .DashboardDest: return "app_dashboard"
            
        case .PreferenceDest: return "app_preference"
            
        case .PermissionDest: return "app_permission"
            
        case .OtaDest: return "app_ota"
            
        case .RideLynkDiscoveryDest: return "app_ridelynk_discovery"
            
        case .RideGridGroupsDest: return "app_ridegrid"
            
        case .RideGridCreateGroupDest: return "app_ridegrid_create_group"
            
        case .RideGridDashboardDest: return "app_ridegrid_dashboard"
            
        case .RideGridInviteRidersDest: return "app_ridegrid_invite_riders"
            
        case .BuddyListDest: return "app_buddy_list"
            
        case .AddBuddiesDest: return "app_add_buddies"
            
        case .SpeedDialEditDest: return "app_speed_dial_edit"
        }
    }
}


private struct BaseViewModelKey: EnvironmentKey {
    static let defaultValue = BluArmorApp.bluArmorContainer.bluArmorAppViewModel
}

extension EnvironmentValues {
    var baseViewModel: BluArmorAppViewModel {
        get { self[BaseViewModelKey.self] }
        set { self[BaseViewModelKey.self] = newValue }
    }
}


struct TestView: View {
    @ObservedObject var vm:BluArmorAppViewModel
    
    var body: some View {
        
        NavigationView{
            VStack{
//                Text("Test \(vm.volumeLevel?.level ?? -1)")
            }
                .navigationBarTitle("", displayMode: .inline)
            
                .navigationBarItems(trailing: Button(action:{self.vm.navigateTo(appDestination: .PreferenceDest)}){
                    Image("preference")
                        .renderingMode(.original)
                        .resizable()
                        .scaledToFit()
                        .frame( height:44)
                    
                })
        }
        
    }
}
