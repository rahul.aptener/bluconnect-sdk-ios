//
//  ColorExtension.swift
//  BluArmor-App
//
//  Created by Rahul Gaur on 20/01/22.
//

import Foundation
import SwiftUI

extension Color {
    init(_ hex: UInt, alpha: Double = 1) {
      self.init(
        .sRGB,
        red: Double((hex >> 16) & 0xFF) / 255,
        green: Double((hex >> 8) & 0xFF) / 255,
        blue: Double(hex & 0xFF) / 255,
        opacity: alpha
      )
    }
    
    init(hex: UInt) {
      self.init(
        .sRGB,
        red: Double((hex & 0x00ff0000) >> 16) / 255,
        green: Double((hex & 0x0000ff00) >> 8) / 255,
        blue: Double(hex & 0x000000ff) / 255,
        opacity: Double((hex & 0xff000000) >> 24) / 255
      )
    }
}
