//
//  BluArmorContainer.swift
//  BluArmor-App
//
//  Created by Rahul Gaur on 24/01/22.
//

import Foundation
import Swinject

final class BluArmorContainer{
    
    private let container: Container
    
    init() {
        self.container = Container()
        registerService()
    }

    
    var bluArmorAppViewModel:BluArmorAppViewModel {
        get{
            container.resolve(BluArmorAppViewModel.self)!
        }
    }

    func registerService() {
        self.container.register(BluArmorAppViewModel.self) { r in
            BluArmorAppViewModel.init()
        }.inObjectScope(.container)
    }
    
}


extension Resolver{
    
    func resolve<T>()->T?{
        if let service = resolve(T.self){
            return service
        } else {
            return nil
        }
    }
}
