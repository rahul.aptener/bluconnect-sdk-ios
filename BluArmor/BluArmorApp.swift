//
//  BluArmorApp.swift
//  BluArmor
//
//  Created by Rahul Gaur on 17/03/22.
//

import SwiftUI
import NavigationStack

@main
struct BluArmorApp {
    public static let bluArmorContainer = BluArmorContainer()
    
    static func main() {
        if #available(iOS 14.0, *) {
            BluArmorAppContainer.main()
        } else {
            UIApplicationMain(
                CommandLine.argc,
                CommandLine.unsafeArgv,
                nil,
                NSStringFromClass(AppDelegate.self))
        }
    }
}

@available(iOS 14.0, *)
struct BluArmorAppContainer: App {
    let persistenceController = PersistenceController.shared
    
    var body: some Scene {
        WindowGroup {
            NavigationView{
                NavigationStackView{
                    ContentView()
                        .environment(\.managedObjectContext, persistenceController.container.viewContext)
                }}.navigationViewStyle(.automatic)
        }
    }
}

struct BluArmorAppContainerCompact:View {
    let persistenceController = PersistenceController.shared
    var body: some View {
            NavigationStackView{
                ContentView()
                    .environment(\.managedObjectContext, persistenceController.container.viewContext)
            }
    }
}

